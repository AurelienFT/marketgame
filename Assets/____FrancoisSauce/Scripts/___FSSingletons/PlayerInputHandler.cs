﻿using System;
using System.Collections;
using System.Collections.Generic;
using FrancoisSauce.Scripts.FSEvents.SO;
using FrancoisSauce.Scripts.FSUtils;
using UnityEngine;

public class PlayerInputHandler : MonoBehaviour
{
    //Player actions
    [SerializeField] private Action moveEvent = null;
    [SerializeField] private Action actionEvent = null;
    [SerializeField] private Action dashEvent = null;

    private bool spacePressed = false;
    private bool movePressed = false;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void OnMoveEvent()
    {
        if (!enabled && isActiveAndEnabled) return;
        if (movePressed) return;

        movePressed = true;
        if (isActiveAndEnabled)
        {
            StartCoroutine(ResetMove());
        }
        moveEvent.Invoke();
    }

    private IEnumerator ResetSpace()
    {
        yield return new WaitForEndOfFrame();

        spacePressed = false;
    }
    
    private IEnumerator ResetMove()
    {
        yield return new WaitForEndOfFrame();

        movePressed = false;
    }
    
    public void OnActionEvent()
    {
        
        if (!enabled && isActiveAndEnabled) return;
        if (spacePressed) return;

        spacePressed = true;
        if (isActiveAndEnabled)
        {
            StartCoroutine(ResetSpace());
        }
        actionEvent.Invoke();
    }
    
    public void OnDashEvent()
    {
        dashEvent.Invoke();
    }
}
 