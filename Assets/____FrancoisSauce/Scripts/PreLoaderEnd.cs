﻿using System.Collections;
using FrancoisSauce.Scripts.FSSingleton;
using FrancoisSauce.Scripts.FSUtils;
using UnityEngine;
using UnityEngine.UI;

//TODO comment
namespace FrancoisSauce.Scripts
{
    public class PreLoaderEnd : MonoBehaviour
    {
        [SerializeField] private GameObject mainAudioListener = null;
        [SerializeField] private GameObject inputManager = null;
        [SerializeField] private GameObject eventSystem = null;

        private IEnumerator Start()
        {
            yield return null;
            
            DontDestroyOnLoad(mainAudioListener);
            DontDestroyOnLoad(inputManager);
            DontDestroyOnLoad(eventSystem);
            
            StartCoroutine(GameManager.Instance.LoadSceneAsync(SceneList.Instance.scenes["MainMenu"], true, false));
            
            yield return new WaitForSecondsRealtime(5f);
                
            GameManager.Instance.UnloadAsync(SceneList.Instance.scenes["PreLoad"]);
        }
    }
}