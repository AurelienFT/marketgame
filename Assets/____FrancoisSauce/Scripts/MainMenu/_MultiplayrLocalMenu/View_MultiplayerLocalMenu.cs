﻿using System;
using System.Collections.Generic;
using FrancoisSauce.Scripts.FSUtils;
using FrancoisSauce.Scripts.FSViews;
using UnityEngine;

namespace FrancoisSauce.Scripts.MainMenu.OptionMenu
{
    /// <summary>
    /// <see cref="Scene_MainMenu"/> implementation of <see cref="IFSView{T}"/> of the OptionMenu
    /// </summary>
    /// <inheritdoc/>
    public class View_MutliplayerLocalMenu : IFSView<Scene_MainMenu>
    {
        //colors are connected players
        private List<GameObject> colors;
        //texts are waiting to connect players
        private List<GameObject> texts;
        
        private int lastConnectedNumber = 0;
        
        /// <summary>
        /// Method to initialize this view
        /// </summary>
        /// <param name="scene">The <see cref="Scene_MainMenu"/> that own this view</param>
        public void Awake(Scene_MainMenu scene)
        {
            var tmp = scene.multiplayerLocalMenuUI.GetComponent<MultiplayerLocalMenuScript>();
            
            colors = tmp.colors;
            texts = tmp.texts;

            tmp.playButton.onClick.AddListener(() =>
            {
                scene.ChangeView(scene.loadingMenu);
                scene.LoadScene(SceneList.Instance.scenes["GameScene"]);
            });
            
            tmp.backButton.onClick.AddListener(() =>
            {
                scene.ChangeView(scene.baseMenu);
                
                scene.currentPlayerNumber = 0;
                lastConnectedNumber = 0;
                scene.ResetPlayer();
            
                for (var i = 0; i < 4; i++)
                {
                    colors[i].SetActive(false);
                    texts[i].SetActive(true);
                }
            });

            foreach (var gameObject in colors)
            {
                gameObject.SetActive(false);
            }
            
            foreach (var gameObject in texts)
            {
                gameObject.SetActive(true);
            }
        }
        
        public override void OnViewEnter(Scene_MainMenu scene)
        {
            scene.multiplayerLocalMenuUI.SetActive(true);
            scene.isInLocalMultiplayer = true;
        }
    
        public override void OnViewExit(Scene_MainMenu scene)
        {
            scene.multiplayerLocalMenuUI.SetActive(false);
            scene.isInLocalMultiplayer = false;
        }
    
        public override void OnUpdate(Scene_MainMenu scene)
        {
            var newConnectedNumber = scene.currentPlayerNumber;
            if (newConnectedNumber == lastConnectedNumber) return;
            
            colors[lastConnectedNumber].SetActive(true);
            texts[lastConnectedNumber].SetActive(false);
            lastConnectedNumber = newConnectedNumber;
        }
    }
}

