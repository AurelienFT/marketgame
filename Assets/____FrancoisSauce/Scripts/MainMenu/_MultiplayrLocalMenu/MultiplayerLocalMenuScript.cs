﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FrancoisSauce.Scripts.MainMenu.OptionMenu
{
    /// <summary>
    /// Container of all the mandatory references for the <see cref="View_OptionMenu"/>
    /// </summary>
    public class MultiplayerLocalMenuScript : MonoBehaviour
    {
        /// <summary>
        /// playButton to allow <see cref="View_OptionMenu"/> to register to his onClick event
        /// </summary>
        public Button playButton;
        //colors are connected players
        public List<GameObject> colors;
        //texts are waiting to connect players
        public List<GameObject> texts;

        public Button backButton;
    }
}

