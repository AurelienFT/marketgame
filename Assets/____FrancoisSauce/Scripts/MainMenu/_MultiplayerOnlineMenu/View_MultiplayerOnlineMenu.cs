using System.Runtime.InteropServices;
using UnityEngine;
using FrancoisSauce.Scripts.FSViews;
using UnityEngine.UI;

namespace FrancoisSauce.Scripts.MainMenu.OptionMenu
{
    /// <summary>
    /// <see cref="Scene_MainMenu"/> implementation of <see cref="IFSView{T}"/> of the MultiplayerOnlineMenu
    /// </summary>
    /// <inheritdoc/>
    public class View_MultiplayerOnlineMenu : IFSView<Scene_MainMenu>
    {
        private Button creationButton = null;
        
        /// <summary>
        /// Method to initialize this view
        /// </summary>
        /// <param name="scene">The <see cref="Scene_MainMenu"/> that own this view</param>
        public void Awake(Scene_MainMenu scene)
        {
            var ui = scene.multiplayerOnlineMenuUI.GetComponent<OnlineMutliplayerScript>();

            creationButton = ui.createLobbyButton;

            scene.steamLobby.buttonCreation = ui.createLobbyButton.gameObject;
            ui.backButton.onClick.AddListener(() =>
            {
                scene.steamLobby.LeaveLobby();
                scene.ChangeView(scene.baseMenu);
            });
            ui.createLobbyButton.onClick.AddListener(() => scene.steamLobby.HostLobby());
        }
        
        public override void OnViewEnter(Scene_MainMenu scene)
        {
            scene.multiplayerOnlineMenuUI.SetActive(true);
        }
    
        public override void OnViewExit(Scene_MainMenu scene)
        {
            creationButton.gameObject.SetActive(true);
            scene.multiplayerOnlineMenuUI.SetActive(false);
        }
    
        public override void OnUpdate(Scene_MainMenu scene)
        {
        }
    }
}
