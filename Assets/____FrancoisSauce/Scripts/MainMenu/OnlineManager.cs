﻿using FrancoisSauce.Scripts.FSUtils;
using FrancoisSauce.Scripts.FSUtils.FSGlobalVariables;
using UnityEngine;

public class OnlineManager : Singleton<OnlineManager>
{
    [SerializeField] private FSGlobalListUlongSO playersIds = null;

    protected override void OnDestroy()
    {
        base.OnDestroy();
        playersIds.value.Clear();
    }
}
