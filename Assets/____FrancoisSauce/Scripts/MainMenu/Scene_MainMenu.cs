﻿using System.Collections.Generic;
using DG.Tweening;
using FrancoisSauce.Scripts.FSSingleton;
using FrancoisSauce.Scripts.FSScenes;
using FrancoisSauce.Scripts.FSUtils;
using FrancoisSauce.Scripts.FSViews;
using FrancoisSauce.Scripts.MainMenu.BaseMenu;
using FrancoisSauce.Scripts.MainMenu.LoadingMenu;
using FrancoisSauce.Scripts.MainMenu.OptionMenu;
using FrancoisSauce.Scripts.FSEvents.SO;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FrancoisSauce.Scripts.MainMenu
{
    /// <summary>
    /// MainMenu implementation of <see cref="IFSScene{T}"/>
    /// </summary>
    /// <inheritdoc/>
    public class Scene_MainMenu : IFSScene<Scene_MainMenu>
    {
        /// <summary>
        /// UI for the BaseMenu <see cref="IFSView{T}"/>
        /// </summary>
        [Header("UIs of the views")]
        [Tooltip("UI for View_BaseMenu")]
        public GameObject baseMenuUI;
        /// <summary>
        /// UI for the OptionMenu <see cref="IFSView{T}"/>
        /// </summary>
        [Tooltip("UI for the View_OptionMenu")]
        public GameObject optionMenuUI;
        /// <summary>
        /// UI for the LoadingMenu <see cref="IFSView{T}"/>
        /// </summary>
        [Tooltip("UI for the View_LoadingMenu")]
        public GameObject loadingMenuUI;
        /// <summary>
        /// UI for the multiplayerLocalMenu <see cref="IFSView{T}"/>
        /// </summary>
        [Tooltip("UI for the View_MultiplayerLocalMenu")]
        public GameObject multiplayerLocalMenuUI;

        /// <summary>
        /// UI for the multiplayerOnlineMenu <see cref="IFSView{T}"/>
        /// </summary>
        [Tooltip("UI for the View_MultiplayerOnlineMenu")]
        public GameObject multiplayerOnlineMenuUI;

        /// <summary>
        /// To keep track of current progression for the progress bar when loading on the loading menu
        /// </summary>
        [HideInInspector]
        public float currentProgression = 0f;
        
        /// <summary>
        /// Finite state machine implementation, BaseMenu
        /// </summary>
        public readonly View_BaseMenu baseMenu = new View_BaseMenu();
        /// <summary>
        /// Finite state machine implementation, OptionMenu
        /// </summary>
        public readonly View_OptionMenu optionMenu = new View_OptionMenu();
        /// <summary>
        /// Finite state machine implementation, LoadingMenu
        /// </summary>
        public readonly View_LoadingMenu loadingMenu = new View_LoadingMenu();

        public readonly  View_MultiplayerOnlineMenu multiplayerOnlineMenu = new View_MultiplayerOnlineMenu();

        public readonly  View_MutliplayerLocalMenu multiplayerMenu = new View_MutliplayerLocalMenu();
    
        /// <summary>
        /// Image for transitioning out of the scene
        /// </summary>
        [Space] public Image transition;
        
        //TODO comment
        [SerializeField] private AudioSource mainMenuMusic = null;
        public AudioMixer mainAudioMixer = null;

        public FSBoolEventSO playMultiplayer = null;
        public SteamLobby steamLobby = null;

        [SerializeField] private List<Transform> parentsPrefabs = null;
    
        /// <summary>
        /// <see cref="MonoBehaviour"/>'s Awake method
        /// Initializing finite state machine
        /// </summary>
        private void Awake()
        {
            steamLobby = GameObject.FindWithTag("SteamLobby").GetComponent<SteamLobby>();
            steamLobby.mainMenu = this;
            steamLobby.playerPrefabParents = parentsPrefabs;

            baseMenu.Awake(this);
            optionMenu.Awake(this);
            loadingMenu.Awake(this);
            multiplayerMenu.Awake(this);
            multiplayerOnlineMenu.Awake(this);
    
            baseMenuUI.SetActive(false);
            optionMenuUI.SetActive(false);
            loadingMenuUI.SetActive(false);
            multiplayerLocalMenuUI.SetActive(false);
            multiplayerOnlineMenuUI.SetActive(false);
            mainMenuMusic.Play();
            
            ResetPlayer();

            ChangeView(baseMenu);
        }

        private void Start()
        {
            EventSystem.current.SetSelectedGameObject(baseMenuUI.GetComponent<BaseMenuScript>().goToMultiplayerButton.gameObject);
        }

        public override void OnSceneLoaded(int index)
        {
            if (index != SceneList.Instance.scenes["GameScene"]) return;
            
            mainMenuMusic.Stop();
            StartCoroutine(GameManager.Instance.EndLoadingActivation(SceneList.Instance.scenes["GameScene"], true));
            transition.DOFade(1, 1f)
                .From(0f)
                .SetEase(Ease.Linear)
                .onComplete += () => GameManager.Instance.UnloadAsync(SceneList.Instance.scenes["MainMenu"]);
        }
    
        public override void OnSceneChanged(int index)
        {
        }
    
        public override void OnLoadProgression(int index, float progression)
        {
            if (index != SceneList.Instance.scenes["GameScene"]) return;
            ChangeView(loadingMenu);
            currentProgression = progression;
        }
    
        public override void OnSceneUnloaded(int index)
        {
        }
    
        public sealed override void OnUpdate()
        {
            currentView.OnUpdate(this);
            GetPlayers();
        }
        
        public override void ChangeView(IFSView<Scene_MainMenu> newView)
        {
            currentView?.OnViewExit(this);
            currentView = newView;
            currentView.OnViewEnter(this);
        }

        /// <summary>
        /// Called from views to change scene when needed.
        /// </summary>
        /// <param name="index">Index of the <see cref="Scene"/> to load.</param>
        public void LoadScene(int index)
        {
            StartCoroutine(GameManager.Instance.LoadSceneAsync(index));
        }
    
        /// <summary>
        /// Called from views to unload scene when needed
        /// </summary>
        /// <param name="index">Index of the <see cref="Scene"/> to unload.</param>
        public void UnloadScene(int index)
        {
        }

        [SerializeField] private int maxPlayerNumber = 4;
        [HideInInspector] public int currentPlayerNumber = 0;
        [HideInInspector] public bool isInLocalMultiplayer = false;

        private void GetPlayers()
        {
            if (!isInLocalMultiplayer) return;
            
            var device = InControl.InputManager.ActiveDevice;

            if (!device.Action1 || currentPlayerNumber >= maxPlayerNumber)
                if (!Input.GetKey(KeyCode.Space)) return;
                else device = null;

            if (CrazyShop.InputManager.Instance.playerDevicesNames.Contains(device)) return;

            currentPlayerNumber++;
            CrazyShop.InputManager.Instance.playerDevicesNames.Add(device);
        }

        public void ResetPlayer()
        {
            currentPlayerNumber = 0;
            CrazyShop.InputManager.Instance.playerDevicesNames.Clear();
        }

        public void OnLeaveLobby()
        {
            ChangeView(baseMenu);
        }
    }
}

