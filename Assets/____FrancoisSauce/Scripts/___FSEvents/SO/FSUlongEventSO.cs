using UnityEngine;
using UnityEngine.Events;

namespace FrancoisSauce.Scripts.FSEvents.SO
{
    /// <summary>
    /// Implementation of <see cref="UnityEvent<ulong>"/> used in <see cref="FSUlongEventSO"/>
    /// </summary>
    [System.Serializable]
    public class FSUlongEvent : UnityEvent<ulong>
    {
    }
    
    /// <inheritdoc cref="FSOneArgumentEventSO{T}>"/>
    [CreateAssetMenu(fileName = "FSUlongEvent", menuName = "FSEvents/FSUlongEvent", order = 1)]
    public class FSUlongEventSO : FSOneArgumentEventSO<ulong>
    {
    }
}