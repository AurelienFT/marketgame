using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace FrancoisSauce.Scripts.FSEvents.SO
{
    /// <summary>
    /// Implementation of <see cref="UnityEvent<List<Vector3>>"/> used in <see cref="FSListVector3EventSO"/>
    /// </summary>
    [System.Serializable]
    public class FSListVector3Event : UnityEvent<List<Vector3>>
    {
    }
    
    /// <inheritdoc cref="FSOneArgumentEventSO{T}>"/>
    [CreateAssetMenu(fileName = "FSListVector3Event", menuName = "FSEvents/FSListVector3Event", order = 1)]
    public class FSListVector3EventSO : FSOneArgumentEventSO<List<Vector3>>
    {
    }
}