﻿using UnityEngine;
using UnityEngine.Events;

namespace FrancoisSauce.Scripts.FSEvents.SO
{
    /// <summary>
    /// Implementation of <see cref="UnityEvent<int, float>"/> used in <see cref="FSStringStringEventSO"/>
    /// </summary>
    [System.Serializable]
    public class FSStringStringEvent : UnityEvent<string, string>
    {
    }
    
    /// <inheritdoc cref="FSTwoArgumentEventSO{T0, T1}>"/>
    [CreateAssetMenu(fileName = "FSStringStringEvent", menuName = "FSEvents/FSStringStringEvent", order = 1)]
    public class FSStringStringEventSO : FSTwoArgumentEventSO<string, string>
    {
    }
}