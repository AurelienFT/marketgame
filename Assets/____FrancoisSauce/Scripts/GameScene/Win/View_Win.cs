﻿using System;
using FrancoisSauce.Scripts.FSSingleton;
using FrancoisSauce.Scripts.FSUtils;
using FrancoisSauce.Scripts.FSViews;
using Steamworks;
using UnityEngine;

namespace FrancoisSauce.Scripts.GameScene.Win
{
    /// <summary>
    /// <see cref="Scene_Game"/> implementation of <see cref="IFSView{T}"/> of the Win view
    /// </summary>
    /// <inheritdoc/>
    public class View_Win : IFSView<Scene_Game>
    {
        /// <summary>
        /// Method to initialize this view
        /// </summary>
        /// <param name="scene">The <see cref="Scene_Game"/> that own this view</param>
        public void Awake(Scene_Game scene)
        {
            var viewUI = scene.winUI.GetComponentInChildren<WinScript>();

            if (scene.isOnline.value)
            {
                viewUI.nextButton.onClick.AddListener(() =>
                {
                    var nbMembers = SteamMatchmaking.GetNumLobbyMembers(new CSteamID(scene.lobbyId.value));
    
                    for (var i = 0; i < nbMembers; i++)
                    {
                        var member = SteamMatchmaking.GetLobbyMemberByIndex(new CSteamID(scene.lobbyId.value), i);
                        
                        var message = new Message
                        {
                            message = "",
                            messageType = MessageTypes.GO_BACK_TO_SELECTION
                        };
                        
                        var buffer = MessageTools.Serialize(message);
                        SteamNetworking.SendP2PPacket(new CSteamID(member.m_SteamID), buffer, (uint)buffer.Length, EP2PSend.k_EP2PSendReliable);
                    }
                });
                if (scene.isServer.value)
                {
                    viewUI.quitButton.onClick.AddListener(() =>
                    {
                        var nbMembers = SteamMatchmaking.GetNumLobbyMembers(new CSteamID(scene.lobbyId.value));
    
                        for (var i = 0; i < nbMembers; i++)
                        {
                            var member = SteamMatchmaking.GetLobbyMemberByIndex(new CSteamID(scene.lobbyId.value), i);
                        
                            var message = new Message
                            {
                                message = "",
                                messageType = MessageTypes.OWNER_LEAVE
                            };
                        
                            var buffer = MessageTools.Serialize(message);
                            SteamNetworking.SendP2PPacket(new CSteamID(member.m_SteamID), buffer, (uint)buffer.Length, EP2PSend.k_EP2PSendReliable);
                        }
                    });
                }
                else
                {
                    viewUI.quitButton.onClick.AddListener(() =>
                    {
                        scene.steamLobby.LeaveLobby();
                    });
                }
            }
            else
            {
                viewUI.nextButton.onClick.AddListener(() =>
                {
                    
                    Debug.Log(scene.levelIndex);
                    GameManager.Instance.UnloadAsync(scene.levelIndex);
                    scene.ChangeView(scene.levelManager);
                });
                
                viewUI.quitButton.onClick.AddListener(() => scene.LoadScene(SceneList.Instance.scenes["MainMenu"]));
            }
        }

        public override void OnViewEnter(Scene_Game scene)
        {
            scene.winUI.SetActive(true);
        }

        public override void OnViewExit(Scene_Game scene)
        {
            scene.winUI.SetActive(false);
        }

        public override void OnUpdate(Scene_Game scene)
        {
        }
    }
}