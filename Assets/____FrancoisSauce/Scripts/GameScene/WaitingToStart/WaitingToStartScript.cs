﻿using UnityEngine;
using UnityEngine.UI;

namespace FrancoisSauce.Scripts.GameScene.WaitingToStart
{
    /// <summary>
    /// Container of all the mandatory references for the <see cref="View_WaitingToStart"/>
    /// </summary>
    public class WaitingToStartScript : MonoBehaviour
    {
    }
}