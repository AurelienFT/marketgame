﻿using FrancoisSauce.Scripts.FSViews;
using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace FrancoisSauce.Scripts.GameScene.WaitingToStart
{
    /// <summary>
    /// <see cref="Scene_Game"/> implementation of <see cref="IFSView{T}"/> of the WaitingToStart view
    /// </summary>
    /// <inheritdoc/>
    public class View_WaitingToStart : IFSView<Scene_Game>
    {
        private bool hasBeenLaunched = false;
        /// <summary>
        /// Method to initialize this view
        /// </summary>
        /// <param name="scene">The <see cref="Scene_Game"/> that own this view</param>
        public void Awake(Scene_Game scene)
        {
        }

        public override void OnViewEnter(Scene_Game scene)
        {
            scene.waitingToStartUI.SetActive(true);
        }

        public override void OnViewExit(Scene_Game scene)
        {
            scene.waitingToStartUI.SetActive(false);
        }
        public IEnumerator StartGame(Scene_Game scene)
        {
            yield return new WaitForSecondsRealtime(3);
            scene.OnClickedStartButton();
            scene.ChangeView(scene.playing);
        }

        public override void OnUpdate(Scene_Game scene)
        {
        }
    }
}