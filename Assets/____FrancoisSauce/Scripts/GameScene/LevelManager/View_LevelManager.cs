﻿using System.Collections;
using System.Collections.Generic;
using FrancoisSauce.Scripts.FSViews;
using FrancoisSauce.Scripts.GameScene;
using UnityEngine;

public class View_LevelManager : IFSView<Scene_Game>
{
    public override void OnViewEnter(Scene_Game scene)
    {
        scene.levelManagerUI.SetActive(true);
    }

    public override void OnViewExit(Scene_Game scene)
    {
        scene.levelManagerUI.SetActive(false);
    }

    public override void OnUpdate(Scene_Game scene)
    {
    }
}
