using UnityEngine;

namespace FrancoisSauce.Scripts.FSUtils.FSGlobalVariables
{
    /// <summary>
    /// <see cref="ulong"/> implementation of the <see cref="FSGlobalVariableSO{T}"/>
    /// </summary>
    /// <inheritdoc/>
    [CreateAssetMenu(fileName = "FSGlobalUlong", menuName = "FSGlobalVariables/FSGlobalUlong", order = 1)]
    public class FSGlobalUlongSO : FSGlobalVariableSO<ulong>
    {
    }
}