﻿using UnityEngine;
using System.Collections.Generic;

namespace FrancoisSauce.Scripts.FSUtils.FSGlobalVariables
{
    /// <summary>
    /// <see cref="List<ulong>"/> implementation of the <see cref="FSGlobalVariableSO{T}"/>
    /// </summary>
    /// <inheritdoc/>
    [CreateAssetMenu(fileName = "FSGlobalListUlong", menuName = "FSGlobalVariables/FSGlobalListUlong", order = 1)]
    public class FSGlobalListUlongSO : FSGlobalVariableSO<List<ulong>>
    {
    }
}