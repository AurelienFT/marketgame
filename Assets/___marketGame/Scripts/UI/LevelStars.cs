﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelStars : MonoBehaviour
{
    [SerializeField] private List<Image> stars = new List<Image>();
    [SerializeField] private List<TextMeshProUGUI> starsValue = new List<TextMeshProUGUI>();
    [SerializeField] private TextMeshProUGUI scoreText = null;

    public void UpdateByLevelIndex(int levelIndex, List<int> goals)
    {
        UpdateByScore(PlayerPrefs.GetInt("Level" + (levelIndex).ToString("0000")), goals);
    }

    public void UpdateByScore(int score, IEnumerable<int> goals)
    {
        var i = 0;

        scoreText.text = score.ToString();
        
        foreach (var goal in goals)
        {
            stars[i].gameObject.SetActive(score > goal);

            starsValue[i].text = goal.ToString();
            i++;
        }
    }
}
