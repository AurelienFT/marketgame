﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using FrancoisSauce.Scripts.FSEvents.SO;
using UnityEngine;
using UnityEngine.UI;

public class UILevelManager : MonoBehaviour
{
    [SerializeField] private Text scoreText = null;
    private int score = 0;

    [SerializeField] private Text eventNameText = null;
    [SerializeField] private Text eventDescriptionText = null;

    [SerializeField] private Text timeText = null;
    public float levelTime = 0f;
    [SerializeField] private FSIntEventSO onTimeUp = null;

    [SerializeField] private LevelStars levelStars = null;
    [SerializeField] private LevelSO levelSettings = null;

    private void Awake()
    {
        scoreText.text = "0";

        var color = eventNameText.color;
        color.a = 0f;
        eventNameText.color = color;
        eventDescriptionText.color = color;
        
        levelStars.gameObject.SetActive(false);
        timeText.text = levelSettings.levelDuration.ToString();
    }

    public void OnScoreChanged(int valueToChange)
    {
        score += valueToChange;
        scoreText.text = score.ToString();
    }

    //TODO maybe update this ? (to add pictures/more infos)
    public void OnEventStart(string eventName, string eventDescription)
    {
        eventNameText.text = eventName;
        eventDescriptionText.text = eventDescription;

        StartCoroutine(ShowHideEventUi());
    }

    private IEnumerator ShowHideEventUi()
    {
        eventNameText.DOFade(.75f, 1f);
        eventDescriptionText.DOFade(.75f, 1f);
        
        yield return new WaitForSecondsRealtime(5f);
        
        eventNameText.DOFade(0f, 1f);
        eventDescriptionText.DOFade(0f, 1f);
    }

    public void OnUpdateGameStarted()
    {
        levelTime -= Time.deltaTime;

        if (levelTime <= 0f)
        {
            onTimeUp.Invoke(score);
            return;
        }
        
        timeText.text = (levelTime < 10 ? Math.Round(levelTime, 2) : (int)levelTime).ToString();
    }

    public void OnTimeUp(int score)
    {
        levelStars.gameObject.SetActive(true);
        levelStars.UpdateByScore(score, levelSettings.levelGoals);
    }
}
