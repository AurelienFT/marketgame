﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class StartingCountdown : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI firstText = null;
    [SerializeField] private TextMeshProUGUI secondText = null;
    [SerializeField] private TextMeshProUGUI thirdText = null;
    
    private void First()
    {
        firstText.DOFade(0, 1)
            .From(1)
            .SetEase(Ease.Linear)
            .onComplete += Second;
        firstText.transform.DOScale(.1f, 1)
            .From(1)
            .SetEase(Ease.Linear);
    }

    private void Second()
    {
        secondText.DOFade(0, 1)
            .From(1)
            .SetEase(Ease.Linear)
            .onComplete += Third;
        secondText.transform.DOScale(.1f, 1)
            .From(1)
            .SetEase(Ease.Linear);
    }

    private void Third()
    {
        thirdText.DOFade(0, 1)
            .From(1)
            .SetEase(Ease.Linear);
        thirdText.transform.DOScale(.1f, 1)
            .From(1)
            .SetEase(Ease.Linear);
    }
    
    private void OnEnable()
    {
        First();
    }

    private void StopTweens()
    {
        firstText.DOKill();
        firstText.transform.DOKill();
        secondText.DOKill();
        secondText.transform.DOKill();
        thirdText.DOKill();
        thirdText.transform.DOKill();
    }

    private void OnDisable()
    {
        StopTweens();
    }

    private void OnDestroy()
    {
        StopTweens();
    }
}
