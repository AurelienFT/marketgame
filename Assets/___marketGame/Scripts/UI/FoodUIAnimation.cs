﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class FoodUIAnimation : MonoBehaviour
{
    private void Awake()
    {
        transform.DORotate(Vector3.up * 360, 2)
            .SetRelative(true)
            .SetLoops(-1)
            .SetEase(Ease.Linear);
    }
}
