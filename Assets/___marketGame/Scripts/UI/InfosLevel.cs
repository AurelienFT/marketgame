﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InfosLevel : MonoBehaviour
{
    [SerializeField] private LevelStars levelStars = null;
    [SerializeField] private TextMeshProUGUI levelNameText = null;
    
    [SerializeField] private List<LevelSO> levelsSettings = new List<LevelSO>();
    
    public void OnLevelSelectionChanged(int levelIndex)
    {
        levelIndex -= 2;//TODO magic number => number of not levels scene in the project
        levelNameText.text = "Level" + (levelIndex).ToString("0000");
        levelStars.UpdateByLevelIndex(levelIndex, levelsSettings[levelIndex - 1].levelGoals);
    }
}
