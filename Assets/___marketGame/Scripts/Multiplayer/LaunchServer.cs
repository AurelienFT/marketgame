﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrancoisSauce.Scripts.MainMenu;
using FrancoisSauce.Scripts.FSUtils.FSGlobalVariables;
using Steamworks;
using System.Text;

public class LaunchServer : MonoBehaviour
{
    [SerializeField] private Scene_MainMenu mainMenu;
    [SerializeField] private FSGlobalUlongSO lobbyId;

    [SerializeField] private FSGlobalListUlongSO players = null;

    public void OnLaunchClicked(string sceneName)
    {
        players.value.Clear();
        var nbMembers = SteamMatchmaking.GetNumLobbyMembers(new CSteamID(lobbyId.value));

        for (var i = 0; i < nbMembers; i++)
        {
            var member = SteamMatchmaking.GetLobbyMemberByIndex(new CSteamID(lobbyId.value), i);
            var seed = 42;
            Random.InitState(seed);
            var launching = new Message {
                message = seed.ToString(),
                messageType = MessageTypes.LAUNCHING
            };
            var buffer = MessageTools.Serialize(launching);
            SteamNetworking.SendP2PPacket(new CSteamID(member.m_SteamID), buffer, (uint)buffer.Length, EP2PSend.k_EP2PSendReliable);
        }
    }
}
