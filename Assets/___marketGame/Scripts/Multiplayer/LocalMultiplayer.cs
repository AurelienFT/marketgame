﻿using System.Runtime.InteropServices;
using UnityEngine;

public class LocalMultiplayer : IMultiplayer
{
    public void ProcessInputs(MultiplayerHandler multiplayerHandler, MultiplayerHandler.PlayerInput playerInput, Vector3 value, int id)
    {
        multiplayerHandler.SendInputs(playerInput, value, id);
    }

    public void GetInputs(MultiplayerHandler multiplayerHandler)
    {
        ; //Should not return anything in local mode
    }

    public void Update(MultiplayerHandler multiplayerHandler)
    {
        ;
    }
}
