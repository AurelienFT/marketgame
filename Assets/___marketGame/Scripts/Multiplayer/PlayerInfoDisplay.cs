﻿using UnityEngine;
using UnityEngine.UI;
using Steamworks;

public class PlayerInfoDisplay : MonoBehaviour
{
    private ulong steamId;

    [SerializeField] private RawImage profileImage = null;
    [SerializeField] private Text displayNameText = null;

    private bool drawed = false;

    protected Callback<AvatarImageLoaded_t> avatarImageLoaded;

    private void Awake()
    {
        ResetPosition();
    }

    public void ResetPosition()
    {
        transform.localPosition = new Vector3(-200, -130, 0);
    }

    public void LoadImage()
    {
        if (drawed) { return; }
        var cSteamId = new CSteamID(this.steamId);
        displayNameText.text = SteamFriends.GetFriendPersonaName(cSteamId);

        var imageId = SteamFriends.GetLargeFriendAvatar(cSteamId);


        if (imageId == -1) { return; }

        profileImage.texture = GetSteamImageAsTexture(imageId);
        drawed = true;
    }
    public void SetSteamId(ulong newSteamId)
    {
        this.steamId = newSteamId;
    }

    public ulong GetSteamId()
    {
        return this.steamId;    
    }

    private void OnAvatarImageLoaded(AvatarImageLoaded_t callback)
    {
        //if(callback.m_steamID.m_SteamID != steamId) { return; }

        var imageId = SteamFriends.GetLargeFriendAvatar(callback.m_steamID);
        profileImage.texture = GetSteamImageAsTexture(imageId);
    }

    private Texture2D GetSteamImageAsTexture(int iImage)
    {
        Texture2D texture = null;

        var isValid = SteamUtils.GetImageSize(iImage, out uint width, out uint height);

        if (!isValid) return texture;
        
        var image = new byte[width * height* 4];

        isValid = SteamUtils.GetImageRGBA(iImage, image, (int)(width * height * 4));

        if (!isValid) return texture;
        
        texture = new Texture2D((int)width, (int)height, TextureFormat.RGBA32, false, true);
        texture.LoadRawTextureData(image);
        texture.Apply();

        return texture;
    }
}
