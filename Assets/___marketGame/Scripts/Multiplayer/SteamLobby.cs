﻿using UnityEngine;
using Steamworks;
using System.Collections.Generic;
using System.Linq;
using FrancoisSauce.Scripts.MainMenu;
using FrancoisSauce.Scripts.FSUtils.FSGlobalVariables;
using FrancoisSauce.Scripts.FSEvents.SO;

public class SteamLobby : MonoBehaviour
{
    public GameObject buttonCreation = null;

    public Scene_MainMenu mainMenu = null;

//    private MyNetworkManager networkManager;

    public List<Transform> playerPrefabParents = new List<Transform>();

    private const string HostAddressKey = "HostAddress";

    protected Callback<LobbyCreated_t> lobbyCreated;
    protected Callback<GameLobbyJoinRequested_t> gameLobbyJoinRequested;
    protected Callback<LobbyEnter_t> lobbyEntered;

    protected Callback<LobbyDataUpdate_t> dataUpdated;

    protected Callback<LobbyChatMsg_t> chatMessage;

    protected Callback<LobbyChatUpdate_t> chatUpdated;

    protected Callback<P2PSessionRequest_t> sessionP2Prequest;

    [SerializeField] private FSGlobalUlongSO lobbyId;

    [SerializeField] private FSGlobalBoolSO isServer;

    [SerializeField] private GameObject playerPrefab;

    public FSBoolEventSO playMultiplayer = null;

    private List<PlayerInfoDisplay> players = new List<PlayerInfoDisplay>();

    private int currentClientNumberInLobby = 0;

    // Start is called before the first frame update
    private void Start()
    {
        if (!SteamManager.Initialized) { return; }    
        isServer.value = false;
        lobbyCreated = Callback<LobbyCreated_t>.Create(OnLobbyCreated);
        gameLobbyJoinRequested = Callback<GameLobbyJoinRequested_t>.Create(OnGameLobbyJoinRequested);
        lobbyEntered = Callback<LobbyEnter_t>.Create(OnLobbyEntered);
        dataUpdated = Callback<LobbyDataUpdate_t>.Create(OnUpdateLobby);
        chatMessage = Callback<LobbyChatMsg_t>.Create(OnReceiveChat);
        chatUpdated = Callback<LobbyChatUpdate_t>.Create(OnUpdateChat);
        sessionP2Prequest = Callback<P2PSessionRequest_t>.Create(OnP2PSessionRequest);
        DontDestroyOnLoad(gameObject);
    }
    
    public void HostLobby()
    {
        buttonCreation.SetActive(false);
        SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypeFriendsOnly, 4);
        isServer.value = true;
    }

    public void LeaveLobby()
    {
        if (isServer.value) {
            var nbMembers = SteamMatchmaking.GetNumLobbyMembers(new CSteamID(lobbyId.value));

            for (var i = 0; i < nbMembers; i++) {
                var member = SteamMatchmaking.GetLobbyMemberByIndex(new CSteamID(lobbyId.value), i);
                var setup = new Message {
                    message = "",
                    messageType = MessageTypes.OWNER_LEAVE
                };
                var buffer = MessageTools.Serialize(setup);
                SteamNetworking.SendP2PPacket(new CSteamID(member.m_SteamID), buffer, (uint)buffer.Length, EP2PSend.k_EP2PSendReliable);
            }
        }
        SteamMatchmaking.LeaveLobby(new CSteamID(lobbyId.value));

        foreach (var playerInfoDisplay in players.Where(playerInfoDisplay => playerInfoDisplay != null && playerInfoDisplay.gameObject != null))
        {
            Destroy(playerInfoDisplay.gameObject);
        }
        
        players.Clear();
    }

    public void LeaveLobbyWithoutMessage()
    {
        SteamMatchmaking.LeaveLobby(new CSteamID(lobbyId.value));

        foreach (var playerInfoDisplay in players.Where(playerInfoDisplay => playerInfoDisplay != null && playerInfoDisplay.gameObject != null))
        {
            Destroy(playerInfoDisplay.gameObject);
        }
        
        players.Clear();
    }

    private void OnUpdateChat(LobbyChatUpdate_t callback)
    {
        //if someone entered the lobby
        if ((callback.m_rgfChatMemberStateChange & (uint)EChatMemberStateChange.k_EChatMemberStateChangeEntered) != 0)
        {
            var setup = new Message {
                message = "setup",
                messageType = MessageTypes.SETUP
            };
            var playerScript = Instantiate(playerPrefab, playerPrefabParents[currentClientNumberInLobby]).GetComponent<PlayerInfoDisplay>();
            playerScript.SetSteamId(callback.m_ulSteamIDUserChanged);
            players.Add(playerScript);
            var buffer = MessageTools.Serialize(setup);
            SteamNetworking.SendP2PPacket(new CSteamID(callback.m_ulSteamIDUserChanged), buffer, (uint)buffer.Length, EP2PSend.k_EP2PSendReliable);
            currentClientNumberInLobby++;
        }
        //if someone leaved the lobby
        else if ((callback.m_rgfChatMemberStateChange & (uint)EChatMemberStateChange.k_EChatMemberStateChangeDisconnected) != 0 || 
            (callback.m_rgfChatMemberStateChange & (uint)EChatMemberStateChange.k_EChatMemberStateChangeKicked) != 0 ||
            (callback.m_rgfChatMemberStateChange & (uint)EChatMemberStateChange.k_EChatMemberStateChangeLeft) != 0 || 
            (callback.m_rgfChatMemberStateChange & (uint)EChatMemberStateChange.k_EChatMemberStateChangeBanned) != 0)
        {
            currentClientNumberInLobby--;
            foreach (var player in players.Where(player => player.GetSteamId() == callback.m_ulSteamIDUserChanged))
            {
                Destroy(player.gameObject);
            }
        
            players.RemoveAll(item => item.GetSteamId() == callback.m_ulSteamIDUserChanged);
            
            for (var i = 0; i < players.Count; i++)
            {
                players[i].transform.SetParent(playerPrefabParents[i]);
                players[i].ResetPosition();
            }
        }
    }

    private void OnP2PSessionRequest(P2PSessionRequest_t callback )
    {
        var nbMembers = SteamMatchmaking.GetNumLobbyMembers(new CSteamID(lobbyId.value));

        for (var i = 0; i < nbMembers; i++)
        {
            var member = SteamMatchmaking.GetLobbyMemberByIndex(new CSteamID(lobbyId.value), i);
            if (new CSteamID(member.m_SteamID) == callback.m_steamIDRemote)
            {
	            SteamNetworking.AcceptP2PSessionWithUser( callback.m_steamIDRemote );
                // Debug.Log("Received a message of type: " + message.messageType + " with text: " + message.message);
            }
        }
    }


    private void OnReceiveChat(LobbyChatMsg_t callback)
    {
        //print(callback.m_eChatEntryType);
    }

    private void OnUpdateLobby(LobbyDataUpdate_t callback)
    {
    }

    private void OnLobbyCreated(LobbyCreated_t callback)
    {
        if (callback.m_eResult != EResult.k_EResultOK)
        {
            print("salope");
            buttonCreation.SetActive(true);
            return;
        }

        lobbyId.value = callback.m_ulSteamIDLobby;
        isServer.value = true;
    }

    private void OnGameLobbyJoinRequested(GameLobbyJoinRequested_t callback)
    {
        SteamMatchmaking.JoinLobby(callback.m_steamIDLobby);
    }

    public void OnPlayerSetup(ulong steamId)
    {
        foreach (
            var playerScript 
            in players.Select(
                player => 
                    player.GetComponent<PlayerInfoDisplay>()).Where(
                        playerScript => playerScript.GetSteamId() == steamId
                        )
            )
        {
            playerScript.LoadImage();
        }
    }

    private void OnLobbyEntered(LobbyEnter_t callback)
    {
        buttonCreation.SetActive(false);
        playMultiplayer.Invoke(true);
        if (!isServer.value)
            mainMenu.ChangeView(mainMenu.multiplayerOnlineMenu);
        lobbyId.value = callback.m_ulSteamIDLobby;
        currentClientNumberInLobby = SteamMatchmaking.GetNumLobbyMembers(new CSteamID(callback.m_ulSteamIDLobby));

        for (var i = 0; i < currentClientNumberInLobby; i++)
        {
            var member = SteamMatchmaking.GetLobbyMemberByIndex(new CSteamID(callback.m_ulSteamIDLobby), i);
            var setup = new Message {
                message = "setup",
                messageType = MessageTypes.SETUP
            };
            var playerScript = Instantiate(playerPrefab, playerPrefabParents[i]).GetComponent<PlayerInfoDisplay>();
            playerScript.SetSteamId(member.m_SteamID);
            players.Add(playerScript);
            var buffer = MessageTools.Serialize(setup);
            SteamNetworking.SendP2PPacket(new CSteamID(member.m_SteamID), buffer, (uint)buffer.Length, EP2PSend.k_EP2PSendReliable);
        }
        //SteamMatchmaking.SendLobbyChatMsg(new CSteamID(lobbyId.value), Encoding.ASCII.GetBytes("hello"), 6);
    }
}
