﻿using FrancoisSauce.Scripts.FSEvents.SO;
using FrancoisSauce.Scripts.FSUtils;
using FrancoisSauce.Scripts.FSUtils.FSGlobalVariables;
using UnityEngine;
using FrancoisSauce.Scripts.FSSingleton;

public class MultiplayerHandler : Singleton<MultiplayerHandler>
{
    public FSGlobalUlongSO lobbyId;
    public FSGlobalListUlongSO players;

    public enum PlayerInput
    {
        Direction,
        Action,
        Dash,
    }

    [SerializeField] private FSGlobalBoolSO isOnline = null;

    private IMultiplayer multiplayerMode;
    
    private readonly LocalMultiplayer localMultiplayer = new LocalMultiplayer();
    private readonly OnlineMultiplayer onlineMultiplayer = new OnlineMultiplayer();

    public FSPlayerInputFloatIntEventSO commandsEvent = null;

    public FSVoidEventSO launchGame = null;
    public FSVoidEventSO leaveLobby = null;
    public FSVoidEventSO nextButton = null;

    public FSUlongEventSO LoadImage = null;

    public FSListVector3EventSO onPlayerSynchronised = null;

    protected override void Awake()
    {
        base.Awake();
            multiplayerMode = null;
        DontDestroyOnLoad(this);
    }

    public void SendInputs(PlayerInput input, Vector3 value, int id)
    {
        commandsEvent.Invoke(input, value, id);
    }

    public void ReceiveInputs(PlayerInput input, Vector3 value, int id)
    {
        multiplayerMode?.ProcessInputs(this, input, value, id);
    }

    public void OnUpdate()
    {
        multiplayerMode?.Update(this);
    }

    public void ChangeScene(string sceneName)
    {
        StartCoroutine (
            GameManager.Instance.LoadSceneAsync(SceneList.Instance.scenes[sceneName])
        );
    }

    public void OnPlayMultiplayer(bool multiplayerOnline)
    {
        isOnline.value = multiplayerOnline;
        multiplayerMode = isOnline.value ? (IMultiplayer) onlineMultiplayer : (IMultiplayer) localMultiplayer;
    }
}
