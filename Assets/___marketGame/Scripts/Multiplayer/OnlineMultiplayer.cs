﻿using UnityEngine;
using Steamworks;
using System;
using FrancoisSauce.Scripts.FSSingleton;
using FrancoisSauce.Scripts.FSUtils;
using FrancoisSauce.Scripts.FSUtils.FSGlobalVariables;

public class OnlineMultiplayer : IMultiplayer
{

    private int nbReady = 0;

    private int nbMembers = 0;
    public void ProcessInputs(MultiplayerHandler multiplayerHandler, MultiplayerHandler.PlayerInput playerInput, Vector3 value, int id)
    {
        var nbMembers = SteamMatchmaking.GetNumLobbyMembers(new CSteamID(multiplayerHandler.lobbyId.value));

        for (var i = 0; i < nbMembers; i++)
        {
            var member = SteamMatchmaking.GetLobbyMemberByIndex(new CSteamID(multiplayerHandler.lobbyId.value), i);
            var message = new Message {
                message = value.x.ToString("0.00") + "&" + value.y.ToString("0.00") + "&" + value.z.ToString("0.00") + "#" + id.ToString(),
                messageType = MessageTypes.DIRECTION
            };

            if (playerInput == MultiplayerHandler.PlayerInput.Action)
            {
                message.message = id.ToString();
                message.messageType = MessageTypes.ACTION;
            }

            if (playerInput == MultiplayerHandler.PlayerInput.Dash)
            {
                message.message = id.ToString();
                message.messageType = MessageTypes.DASH;
            }
            
            //Debug.Log("Player input value : " + value);
            //Debug.Log("value sended over messages : " + message.message);
            var buffer = MessageTools.Serialize(message);
            SteamNetworking.SendP2PPacket(new CSteamID(member.m_SteamID), buffer, (uint)buffer.Length, EP2PSend.k_EP2PSendReliable);
        }
    }

    public void GetInputs(MultiplayerHandler multiplayerHandler)
    {
        throw new System.NotImplementedException();
    }

    public void Update(MultiplayerHandler multiplayerHandler) 
    {
        uint size;
        // repeat while there's a P2P message available
        // will write its size to size variable
        while (SteamNetworking.IsP2PPacketAvailable(out size))
        {
            // allocate buffer and needed variables
            //TODO: c dla merde
            var buffer = new byte[1000];
            CSteamID remoteId;
            // read the message into the buffer
            if (SteamNetworking.ReadP2PPacket(buffer, 1000, out size, out remoteId))
            {
                buffer[size] = 0;
                //foreach(var item in buffer)
                //{
                //    Debug.Log(Convert.ToChar(item));
                //}
    
                var message = MessageTools.Deserialize(buffer);
                if (message.messageType == MessageTypes.SETUP || message.messageType == MessageTypes.READY )
                    Debug.Log("Received a message of type: " + message.messageType + " from: " + remoteId.m_SteamID + " with text: " + message.message);

                switch (message.messageType)
                {
                    case MessageTypes.SETUP:
                        multiplayerHandler.LoadImage.Invoke(remoteId.m_SteamID);
                        break;
                        //setsteamid remote id
                    case MessageTypes.LAUNCHING:
                        nbMembers = SteamMatchmaking.GetNumLobbyMembers(new CSteamID(multiplayerHandler.lobbyId.value));
                        for (var i = 0; i < nbMembers; i++)
                        {
                                var member = SteamMatchmaking.GetLobbyMemberByIndex(new CSteamID(multiplayerHandler.lobbyId.value), i);
                                multiplayerHandler.players.value.Add(member.m_SteamID);
                        }
                        UnityEngine.Random.InitState(Convert.ToInt32(message.message));
                        multiplayerHandler.ChangeScene("GameScene");
                        break;
                    case MessageTypes.DIRECTION:
                        var infos = message.message.Split('#');
                        var direction = MessageTools.DeserializeDirection(infos[0]);
                        //Debug.Log("Received direction: " + direction);
                        multiplayerHandler.commandsEvent.Invoke(MultiplayerHandler.PlayerInput.Direction, direction, int.Parse(infos[1]));
                        break;
                    case MessageTypes.ACTION:
                        //Debug.Log("connard");
                        multiplayerHandler.commandsEvent.Invoke(MultiplayerHandler.PlayerInput.Action, Vector3.zero, int.Parse(message.message));
                        break;
                    case MessageTypes.DASH:
                        multiplayerHandler.commandsEvent.Invoke(MultiplayerHandler.PlayerInput.Dash, Vector3.zero, int.Parse(message.message));
                        break;
                    case MessageTypes.READY:
                        nbReady += 1;
                        if (nbReady == nbMembers) {
                            multiplayerHandler.launchGame.Invoke();
                        }
                        break;
                    case MessageTypes.SYNC_PLAYERS:
                        var playersInfo = MessageTools.DeserializeSyncPlayers(message.message);
                        multiplayerHandler.onPlayerSynchronised.Invoke(playersInfo);
                        break;
                    case MessageTypes.OWNER_LEAVE:
                        multiplayerHandler.leaveLobby.Invoke();
                        nbReady = 0;
                        SteamMatchmaking.LeaveLobby(new CSteamID(multiplayerHandler.lobbyId.value));
                        break;
                    case MessageTypes.GO_BACK_TO_SELECTION:
                        nbReady = 0;
                        multiplayerHandler.nextButton.Invoke();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
