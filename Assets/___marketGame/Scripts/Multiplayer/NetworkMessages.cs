using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;
using UnityEngine;
using System.Text;
public enum MessageTypes
{
    SETUP = 0,
    LAUNCHING = 1,
    DIRECTION = 2,
    ACTION = 3,
    DASH = 4,
    READY = 5,
    SYNC_PLAYERS = 6,
    OWNER_LEAVE = 7,
    GO_BACK_TO_SELECTION = 8,
}

public struct Message
{
    public MessageTypes messageType;
    public string message;
}

public static class MessageTools
{
    /// <summary>
    /// converts byte[] to struct
    /// </summary>
    public static Message Deserialize(byte[] rawData)
    {
        //MessageType messageType;
        string[] dataSplited = Encoding.Default.GetString(rawData).Split('|');
        //foreach(var item in dataSplited)
        //{
        //    Debug.Log(item);
        //}
        if (dataSplited.Length != 2) {
            throw new Exception();
        }
        MessageTypes messageType = (MessageTypes)dataSplited[0][0];
        string messageText = dataSplited[1];
        
        Debug.Log((int)messageType);
        return new Message {messageType = messageType, message = messageText};
    }

    // /// <summary>
    // /// converts Message to byte[]
    // /// </summary>
    public static byte[] Serialize(Message message)
    {
        List<byte> ret = new List<byte>();
        //Debug.Log("message = " + message.message);
        //Debug.Log("message length = " + message.message.Length);
        byte type = (byte)message.messageType;
        ret.Add(type);
        ret.Add(Convert.ToByte('|'));
        ret.AddRange(Encoding.ASCII.GetBytes(message.message));
        //ret.Add(Convert.ToByte('\0'));
        //foreach(var item in ret)
        //{
        //    Debug.Log(Convert.ToChar(item));
        //}
        return ret.ToArray();
    }

    public static Vector3 DeserializeDirection(string direction)
    {
        string[] positions = direction.Split('&');
        if (positions.Length != 3)
        {
            throw new Exception();
        }
        return new Vector3(float.Parse(positions[0]), float.Parse(positions[1]), float.Parse(positions[2]));
    }

    public static List<Vector3> DeserializeSyncPlayers(string playersInfo)
    {
        var playersPositions = new List<Vector3>();
        string[] players = playersInfo.Split('&');
        foreach (var player in players)
        {
            string[] playerPosition = player.Split('@');
            if (playerPosition.Length != 3)
            {
                throw new Exception();
            }
            playersPositions.Add(new Vector3(float.Parse(playerPosition[0]), float.Parse(playerPosition[1]), float.Parse(playerPosition[2])));
        }
        return playersPositions;
    }
}