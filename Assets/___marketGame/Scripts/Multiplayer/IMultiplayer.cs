﻿using UnityEngine;

public interface IMultiplayer
{
    void ProcessInputs(MultiplayerHandler multiplayerHandler, MultiplayerHandler.PlayerInput playerInput,
        Vector3 value, int id);

    void Update(MultiplayerHandler multiplayerHandler);
    //TODO change this to return a list of inputs and ids
    void GetInputs(MultiplayerHandler multiplayerHandler);
}
