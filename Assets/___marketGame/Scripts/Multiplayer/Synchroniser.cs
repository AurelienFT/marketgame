﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrancoisSauce.Scripts.FSUtils.FSGlobalVariables;
using Steamworks;

public class Synchroniser : MonoBehaviour
{
    [SerializeField] private FSGlobalBoolSO isServer;

    [SerializeField] private List<GameObject> players;

    [SerializeField] private FSGlobalUlongSO lobbyId;

    private List<CSteamID> otherIds = new List<CSteamID>();

    private void Awake()
    {
        if (isServer.value)
        {
            var mySteamId = SteamUser.GetSteamID();
            var nbMembers = SteamMatchmaking.GetNumLobbyMembers(new CSteamID(lobbyId.value));
            for (int i = 0; i < nbMembers; i++)
            {
                var member = SteamMatchmaking.GetLobbyMemberByIndex(new CSteamID(lobbyId.value), i);
                if (mySteamId.m_SteamID != member.m_SteamID)
                    otherIds.Add(new CSteamID(member.m_SteamID));
            }
        }
    }

    private void Update()
    {
        if (isServer.value)
        {
            var messageContent = "";
            var first = true;
            foreach (var player in players)
            {
                var position = player.transform.position;
                if (first)
                {
                    messageContent += position.x.ToString() + "@" + position.y.ToString() + "@" + position.z.ToString();
                    first = false;
                }
                else
                {
                    messageContent += "&" + position.x.ToString() + "@" + position.y.ToString() + "@" + position.z.ToString();
                }
            }
            Message sync = new Message {
                message = messageContent,
                messageType = MessageTypes.SYNC_PLAYERS
            };
            byte[] buffer = MessageTools.Serialize(sync);
            foreach (var idMember in otherIds)
            {
                SteamNetworking.SendP2PPacket(idMember, buffer, (uint)buffer.Length, EP2PSend.k_EP2PSendReliable);
            }
        }
    }

    public void OnPlayerSynchronised(List<Vector3> newPosition)
    {
        for(int i = 0; i < newPosition.Count; i++)
        {
            players[i].transform.position = newPosition[i];
        }
    }
}
