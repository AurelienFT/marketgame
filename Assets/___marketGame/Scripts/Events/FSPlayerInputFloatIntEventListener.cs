﻿using System;
using System.Collections.Generic;
using FrancoisSauce.Scripts.FSEvents.SO;
#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif
using System.Reflection;
#if UNITY_EDITOR
using UnityEditor.Events;
#endif
using UnityEngine;
using UnityEngine.Events;

namespace FrancoisSauce.Scripts.FSEvents.Listeners
{
    public class FSPlayerInputFloatIntEventListener : IFSThreeArgumentEventListener<MultiplayerHandler.PlayerInput, Vector3, int>
    {
        [Tooltip("The <int float> event to listen")]
        public FSPlayerInputFloatIntEventSO fsPlayerInputFloatIntSo;

        [Tooltip("The methods to be called when above event is raised")]
        [SerializeField] public FSPlayerInputFloatIntEvent listeners = new FSPlayerInputFloatIntEvent();
        
#if UNITY_EDITOR
#if ODIN_INSPECTOR
        [Button("Auto Add")]
        [ContextMenu("Auto Add")]
#endif
        private void AutoAdd()
        {
            // Keeping track of already registered events
            var listenersName = new List<string>();
            var listenersTarget = new List<Type>();

            for (var i = 0; i < listeners.GetPersistentEventCount(); i++)
            {
                listenersName.Add(listeners.GetPersistentMethodName(i));
                listenersTarget.Add(listeners.GetPersistentTarget(i).GetType());
            }
            
            // Find all behaviours in the current gameObject
            var allBehaviours = GetComponents<MonoBehaviour>();
            // Keep track of event name
            var eventName = fsPlayerInputFloatIntSo.name.Replace(" ", "");

            foreach (var monoBehaviour in allBehaviours)
            {
                if (monoBehaviour == this) continue;
                
                // Get all method names in the behaviour
                MethodInfo[] methodName = monoBehaviour.GetType().GetMethods();
                foreach (var methodInfo in methodName)
                {
                    // Reject non equal names
                    if (eventName != methodInfo.Name) continue;
                    // Reject already registered events
                    if (listenersTarget.Contains(monoBehaviour.GetType()) && listenersName.Contains(methodInfo.Name)) continue;

                    // If not rejected create new delegate and finish the method
                    var action = methodInfo.CreateDelegate(typeof(UnityAction), monoBehaviour) as UnityAction<MultiplayerHandler.PlayerInput, Vector3, int>;
                    UnityEventTools.AddPersistentListener(listeners, action);
                }
            }
        }
#endif

        

        #region Listener Handler
        
        protected override void AddListeners()
        {
            fsPlayerInputFloatIntSo.fsThreeArgumentAction += Invoke;
        }

        protected override void RemoveListeners()
        {
            fsPlayerInputFloatIntSo.fsThreeArgumentAction -= Invoke;
        }

        protected override void Invoke(MultiplayerHandler.PlayerInput valueOne, Vector3 valueTwo, int valueThree)
        {
            listeners.Invoke(valueOne, valueTwo, valueThree);
        }

        #endregion
        
        #region Unity Functions

        protected override void Awake()
        {
#if UNITY_EDITOR
            if (listeners.GetPersistentEventCount() == 0)
                AutoAdd();
#endif

            RemoveListeners();
            AddListeners();
        }

        protected override void OnEnable()
        {
            RemoveListeners();
            AddListeners();
        }

        protected override void OnDisable()
        {
            RemoveListeners();
        }

        protected override void OnDestroy()
        {
            RemoveListeners();
        }

        #endregion
    }
}