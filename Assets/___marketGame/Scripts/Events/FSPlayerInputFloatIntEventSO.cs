﻿using UnityEngine;
using UnityEngine.Events;

namespace FrancoisSauce.Scripts.FSEvents.SO
{
    /// <summary>
    /// Implementation of <see cref="UnityEvent<MultiplayerHandler.PlayerInput, float, int>"/> used in <see cref="FSIntEventSO"/>
    /// </summary>
    [System.Serializable]
    public class FSPlayerInputFloatIntEvent : UnityEvent<MultiplayerHandler.PlayerInput, Vector3, int>
    {
    }
    
    /// <inheritdoc cref="FSThreeArgumentEventSO{T0, T1, T3}>"/>
    [CreateAssetMenu(fileName = "FSPlayerInputFloatIntEventSO", menuName = "FSEvents/FSPlayerInputFloatIntEvent", order = 1)]
    public class FSPlayerInputFloatIntEventSO : FSThreeArgumentEventSO<MultiplayerHandler.PlayerInput, Vector3, int>
    {
        public void Invoke(MultiplayerHandler.PlayerInput input, Vector3 value, int id)
        {
            fsThreeArgumentAction?.Invoke(input, value, id);
        }
    }
}