﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckoutManager : MonoBehaviour
{
    [SerializeField] private List<Checkout> checkouts;
    [SerializeField] private int maxClientsPerCheckout = 5;

    private void Awake()
    {
        
    }

    public Vector3 AddToCheckout(CustomerStateManager customer)
    {
        var min = checkouts.Min(value => value.customers.Count);
        var checkout = checkouts.First(value => value.customers.Count == min);

        return checkout.AddCustomerToQueue(customer);
    }
}
