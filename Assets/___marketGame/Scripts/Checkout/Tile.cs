﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Tile : MonoBehaviour
{
    [HideInInspector] public Vector3 cursorPosition = Vector3.zero;
    [HideInInspector] public float mostLeftPosition = 0;
    public Image myRenderer = null;

    private Transform myTransform = null;

    private void Awake()
    {
        myTransform = transform;
    }

    private void OnEnable()
    {
        myRenderer.enabled = true;
    }

    public bool UpdatePosition()
    {
        myTransform.position += Vector3.left * Time.deltaTime * 2.5f;

        if (!(myTransform.position.x < mostLeftPosition))
        {
            return true;
        }
        myRenderer.enabled = false;
        return false;
    }

    public int OnAction()
    {
        myRenderer.enabled = false;
        print(Vector3.Distance(cursorPosition, myTransform.position) <= 1 ? 10 : -10);
        return Vector3.Distance(cursorPosition, myTransform.position) <= 1 ? 10 : -10;
    }
}
