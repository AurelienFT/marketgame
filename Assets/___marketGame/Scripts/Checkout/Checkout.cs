﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FrancoisSauce.Scripts.FSEvents.SO;
using FrancoisSauce.Scripts.FSUtils;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class Checkout : IInteractible
{
    [SerializeField] private GameObject ui = null;
    [SerializeField] private Transform cursor = null;
    [SerializeField] private Transform startingPosition = null;
    [SerializeField] private Transform endingPosition = null;
    public List<Tile> tiles = new List<Tile>();

    private int tilesCreated = 0;
    [SerializeField] private int maxTiles = 0;

    private bool isActionReady = true;

    [HideInInspector] public int index = 0;

    [SerializeField] private Transform firstPlace = null;
    [SerializeField] private Transform secondPlace = null;
    private Vector3 direction = Vector3.zero;

    [HideInInspector] public List<CustomerStateManager> customers = new List<CustomerStateManager>();


    private const float minTimeBetweenTiles = .5f;
    private const float maxTimeBetweenTiles = 1.5f;
    private float timeBetweenTiles = 0f;
    private float currentTimeBetweenTiles = 0f;

    [SerializeField] private FSIntEventSO onScoreChanged = null;

    [SerializeField] private GameObjectPool tilesPool = null;
    [SerializeField] private AudioSource badSound = null;
    [SerializeField] private AudioSource goodSound = null;
    [SerializeField] private ParticleSystem badParticles1 = null;
    [SerializeField] private ParticleSystem badParticles2 = null;
    [SerializeField] private ParticleSystem goodParticles1 = null;
    [SerializeField] private ParticleSystem goodParticles2 = null;
    
    private void Awake()
    {
        direction = secondPlace.position - firstPlace.position;
        timeBetweenTiles = Random.Range(minTimeBetweenTiles, maxTimeBetweenTiles);
    }

    public Vector3 AddCustomerToQueue(CustomerStateManager customer)
    {
        customers.Add(customer);
        
        return (firstPlace.position + direction * (customers.Count - 1));
    }

    private void OnCustomerLeaveCheckout()
    {
        var i = 0;
        
        customers.RemoveAt(0);
        foreach (var customer in customers)
        {
            customer.MoveCustomer(firstPlace.position + direction * i);
            i++;
        }

        maxTiles = customers[0].foodNumber;
    }

    public override void InteractStart(Transform player)
    {
        ui.SetActive(true);
        isInteracted = true;

        if (customers.Count <= 0)
            return;
        if (Vector3.Distance(customers[0].transform.position, customers[0].navMeshAgent.destination) >= 2.01f)
            return;

        maxTiles = customers[0].foodNumber;
    }

    private void CreateTile()
    {
        currentTimeBetweenTiles += Time.deltaTime;
        if (currentTimeBetweenTiles < timeBetweenTiles) return;
        
        currentTimeBetweenTiles = 0f;
        timeBetweenTiles = Random.Range(minTimeBetweenTiles, maxTimeBetweenTiles);
        
        tilesCreated++;

        var newTile = tilesPool.RequestPool().GetComponent<Tile>();

        newTile.cursorPosition = cursor.position;
        newTile.mostLeftPosition = endingPosition.position.x;
        newTile.transform.position = startingPosition.position;
        
        tiles.Add(newTile);
    }

    public override bool InteractStay()
    {
        if (customers.Count <= 0)
            return true;
        var tmp = customers[0].transform.position - customers[0].navMeshAgent.destination;
        tmp.y = 0;
        if (tmp.magnitude >= 2.01f || tmp.magnitude <= -2.01f)
            return true;

        if (tilesCreated < maxTiles)
        {
            CreateTile();
        }
        
        foreach (var tile in tiles.Where(tile => !tile.UpdatePosition()))
        {
            UpdateScore(-10);
        }
        
        CheckForEndCustomer();
        
        return true;
    }

    public override void InteractEnd()
    {
        ui.SetActive(false);
        isInteracted = false;
    }
    
    private IEnumerator ResetAction()
    {
        yield return new WaitForSeconds(.3f);

        isActionReady = true;
    }

    private void UpdateScore(int score)
    {
        if (score >= 0)
        {
            if (goodParticles1.isPlaying)
                goodParticles2.Play();
            else
                goodParticles1.Play();
            goodSound.Play();
        }
        else
        {
            if (badParticles1.isPlaying)
                badParticles2.Play();
            else
                badParticles1.Play();
            badSound.Play();
        }
        onScoreChanged.Invoke(score);
    }

    public override void InteractAction()
    {
        if (!isActionReady) return;

        isActionReady = false;
        StartCoroutine(ResetAction());

        UpdateScore(tiles[0].OnAction() * customers[0].happinessMultiplier);
        CheckForEndCustomer();
    }

    private void Update()
    {
        customers.RemoveAll(manager => manager.navMeshAgent.speed > 3.6f);

        var i = 0;
        foreach (var customer in customers)
        {
            customer.MoveCustomer(firstPlace.position + direction * i);
            i++;
        }
    }

    private void CheckForEndCustomer()
    {
        foreach (var tile in tiles.Where(tile => tile.myRenderer.enabled == false))
        {
            tile.gameObject.SetActive(false);
        }

        tiles.RemoveAll(tile => tile.isActiveAndEnabled == false);

        if (tiles.Count != 0 || tilesCreated != maxTiles) return;

        print(direction);
        customers[0].navMeshAgent.destination -= direction * 10;
        StartCoroutine(customers[0].Quitting());
        tilesCreated = 0;
        OnCustomerLeaveCheckout();
    }
}
