﻿using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;
using FrancoisSauce.Scripts.FSEvents.SO;

[CreateAssetMenu(fileName = "Level0001", menuName = "CrazyShop/Level/LevelSettings", order = 1)]
public class LevelSO : ScriptableObject
{
    #region Level settings

    [Header("Level settings")] public string levelName = "Level0001";
    public float levelDuration;
    public List<int> levelGoals = new List<int>(3);
        
    #endregion

    #region Customer settings
    [Header("Customer settings")]

    public int maxCustomerNumberAtSameTime = 0;

    public float minTimeCustomerSpawnRate = 0f;
    public float maxTimeCustomerSpawnRate = 0f;
    public float minTimeCustomerOopsiesRate = 0f;
    public float maxTimeCustomerOopsiesRate = 0f;
    
    //TODO add here fields to handle scoring

    #endregion

    #region Box settings
    [Header("Box settings")]

    public int boxNumber = 0;

    public int minFoodNumberInBox = 0;
    public int maxFoodNumberInBox = 0;
    
    //TODO add here fields to handle scoring

    #endregion

    #region Fruits settings

    public List<Food.FoodType> availableFoodTypes = new List<Food.FoodType>();

    #endregion

    #region Events settings

    public readonly List<Func<IEnumerator>> events = new List<Func<IEnumerator>>();
    public bool isEventOccuring = false;
    public float minTimeEventSpawnRate = 0;
    public float maxTimeEventSpawnRate = 0;
    
    //Initial values
    private bool initialIsEventOccuring = false;
    private int initialMaxCustomerNumberAtSameTime = 0;
    private int initialBoxNumber = 0;

    [SerializeField] private FSStringStringEventSO onEventStarted = null;

    //TODO add more events
    public void SetUpList()
    {
        //events.Add(MaxNumberClientUpEvent);
        events.Add(NewFoodBoxesAvailable);
        
        initialIsEventOccuring = isEventOccuring;
        initialMaxCustomerNumberAtSameTime = maxCustomerNumberAtSameTime;
        initialBoxNumber = boxNumber;
    }

    public void ResetObject()
    {
        events.Clear();
        isEventOccuring = initialIsEventOccuring;
        maxCustomerNumberAtSameTime = initialMaxCustomerNumberAtSameTime;
        boxNumber = initialBoxNumber;
    }

    private IEnumerator MaxNumberClientUpEvent()
    {
        onEventStarted.Invoke("$$ More people = more profits $$", "Max client number in the shop raised by 3,\ngood luck ;)");
        isEventOccuring = true;
        maxCustomerNumberAtSameTime += 3;
        
        yield return new WaitForSecondsRealtime(30);
        maxCustomerNumberAtSameTime -= 3;

        isEventOccuring = false;
    }

    private IEnumerator NewFoodBoxesAvailable()
    {
        yield return new WaitForEndOfFrame();
        onEventStarted.Invoke("New product boxes available", "It's time to refill hte shelves");
        boxNumber += 3;
    }

    #endregion
}