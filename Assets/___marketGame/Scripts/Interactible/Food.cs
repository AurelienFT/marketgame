﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using FrancoisSauce.Scripts.FSEvents.SO;
using RootMotion.FinalIK;
using UnityEngine;

public class Food : IInteractible
{
    private FoodPosition foodPosition;
    private bool isTriggeringAFoodPosition = false;

    private FixedJoint joint = null;

    private BipedIK bipedIk = null;

    [SerializeField] private FSIntEventSO onScoreChanged = null;

    [SerializeField] private FoodType type;
    [SerializeField] private List<Mesh> foodMeshes = new List<Mesh>();
    [SerializeField] private MeshFilter myMesh = null;
    [SerializeField] private List<MeshCollider> foodColliders = new List<MeshCollider>();

    public enum FoodType
    {
        Banana = 0,
        Apple = 1,
        Sushi = 2,
        Jean = 3,
        Shoe = 4,
    }

    public void SetFoodType(FoodType newFoodType)
    {
        type = newFoodType;
        foreach (var foodCollider in foodColliders)
        {
            foodCollider.enabled = false;
        }

        print((int) type);
        myMesh.mesh = foodMeshes[(int) type];
        foodColliders[(int)type].enabled = true;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != 13) return; //BoxPosition layer

        foodPosition = other.GetComponent<FoodPosition>();

        if (foodPosition.foodType != type) return;

        isTriggeringAFoodPosition = true;
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != 13) return; //BoxPosition layer
        if (other.gameObject.GetComponent<FoodPosition>().foodType != type) return;

        foodPosition = null;
        isTriggeringAFoodPosition = false;
    }

    public override void InteractStart(Transform player)
    {
        isInteracted = true;
        
        bipedIk = player.GetComponent<PlayerInteraction>().bipedIk;
        bipedIk.enabled = true;
        transform.parent = player;
        transform.DOLocalMove(Vector3.forward * 1.5f, .2f);
        transform.DOLocalRotate(Vector3.zero, .2f)
            .onComplete += () =>
        {
            joint = gameObject.AddComponent<FixedJoint>();
            joint.connectedBody = player.GetComponent<Rigidbody>();
        };
    }

    public override bool InteractStay()
    {
        return isInteracted;
    }

    public override void InteractEnd()
    {
        isInteracted = false;
        transform.parent = null;
        bipedIk.enabled = false;

        transform.DOKill();
        Destroy(joint);
        
        if (!isTriggeringAFoodPosition) return;

        if (foodPosition.AddFood() != true) return;
        isTriggeringAFoodPosition = false;
        gameObject.SetActive(false);
        
        //TODO change this to be configurable ?
        onScoreChanged.Invoke(10);
    }

    public override void InteractAction()
    {
        isInteracted = false;
    }
}