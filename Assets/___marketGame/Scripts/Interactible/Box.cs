﻿using DG.Tweening;
using FrancoisSauce.Scripts.FSEvents.SO;
using RootMotion.FinalIK;
using UnityEngine;

public class Box : IInteractible
{
    public BoxPosition boxPosition;
    private bool isTriggeringABoxPosition = false;

    [SerializeField] private BoxOpened openedBox = null;

    [SerializeField] private Rigidbody myRigidbody = null;

    private BipedIK bipedIk = null;
 
    private FixedJoint joint = null;

    [SerializeField] private FSIntEventSO onScoreChanged = null;

    private void OnEnable()
    {
        myRigidbody.constraints = RigidbodyConstraints.None;
    }

    private void OnTriggerEnter(Collider other)
    {
        //TODO add something ato avoid colision between player and box
        //TODO add the creation of a fixed joint to block the box

        if (other.gameObject.layer != 12) return; //BoxPosition layer

        boxPosition = other.gameObject.GetComponent<BoxPosition>();

        if (boxPosition.isEmpty == false) return;

        isTriggeringABoxPosition = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != 12) return; //BoxPosition layer
        
        isTriggeringABoxPosition = false;
    }

    public override void InteractStart(Transform player)
    {
        isInteracted = true;

        bipedIk = player.GetComponent<PlayerInteraction>().bipedIk;
        bipedIk.enabled = true;
        transform.parent = player;
        transform.DOLocalMove(Vector3.forward * 1.25f, .2f);
        transform.DOLocalRotate(Vector3.zero, .2f)
            .onComplete += () =>
            {
                joint = gameObject.AddComponent<FixedJoint>();
                joint.connectedBody = player.GetComponent<Rigidbody>();
            };
    }

    public override bool InteractStay()
    {
        return isInteracted;
    }

    public override void InteractEnd()
    {
        isInteracted = false;
        transform.parent = null;

        bipedIk.enabled = false;
        
        transform.DOKill();
        Destroy(joint);

        if (isTriggeringABoxPosition == false) return;

        isTriggeringABoxPosition = false;
        transform.DOMove(boxPosition.transform.position, .5f)
            .SetEase(Ease.Linear);
        transform.DORotate(Vector3.zero, .5f)
            .SetEase(Ease.Linear);
        myRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        boxPosition.isEmpty = false;
        openedBox.enabled = true;
        enabled = false;
        
        //TODO update this by settings
        onScoreChanged.Invoke(10);
    }

    public override void InteractAction()
    {
        isInteracted = false;
    }
}
