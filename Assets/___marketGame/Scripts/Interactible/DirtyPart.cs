﻿using System;
using FrancoisSauce.Scripts.FSEvents.SO;
using FrancoisSauce.Scripts.FSUtils;
using UnityEngine;

public class DirtyPart : IInteractible
{
    public float timeToFinish = 2f;
    private float currentTime = 0f;

    [SerializeField] public Renderer myRenderer = null;

    [SerializeField] private FSIntEventSO onScoreChanged = null;

    [SerializeField] private GameObject particlesGameObject = null;

    private void OnEnable()
    {
        myRenderer.enabled = true;
        particlesGameObject.SetActive(true);
    }

    private void OnDisable()
    {
        myRenderer.enabled = false;
        enabled = false;
    }

    public override void InteractStart(Transform player)
    {
        isInteracted = true;
    }

    public override bool InteractStay()
    {
        currentTime += Time.deltaTime;

        return !(currentTime >= timeToFinish);
    }

    public override void InteractEnd()
    {
        isInteracted = false;
        
        if (!(currentTime >= timeToFinish)) return;
        
        onScoreChanged.Invoke(10);
        transform.parent.gameObject.SetActive(false);
    }

    public override void InteractAction()
    {
        ;//Nothing happen when pressing action while cleaning dirty part
    }
}
