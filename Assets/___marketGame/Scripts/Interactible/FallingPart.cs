﻿using DG.Tweening;
using UnityEngine;

public class FallingPart : MonoBehaviour
{
    [SerializeField] private Rigidbody myRigidbody = null;
    [SerializeField] private DirtyPart dirtyPart = null;
    [SerializeField] private Renderer myRenderer = null;
    [SerializeField] private BoxCollider myCollider = null;

    private void OnEnable()
    {
        myRigidbody.constraints = RigidbodyConstraints.None;
        myRigidbody.angularVelocity = new Vector3(Random.Range(0, 90), Random.Range(0, 90), Random.Range(0, 90));
        myRigidbody.AddForce(new Vector3(1, 1, 0));
        myRenderer.enabled = true;
        dirtyPart.enabled = false;
    }

    private void OnDisable()
    {
        myRenderer.enabled = false;
        myRigidbody.constraints = RigidbodyConstraints.FreezeAll;
        dirtyPart.enabled = true;
        transform.rotation = Quaternion.identity;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer != 10) return;
 
        enabled = false;
        myCollider.enabled = false;
        dirtyPart.transform.DOMoveY(-.45f, .01f)
            .SetEase(Ease.Linear);
    }
}
