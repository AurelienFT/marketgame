﻿using UnityEngine;

public abstract class IInteractible : MonoBehaviour
{
    public bool isInteracted = false;
    
    public abstract void InteractStart(Transform player);

    public abstract bool InteractStay();

    public abstract void InteractEnd();
    
    public abstract void InteractAction();
}
