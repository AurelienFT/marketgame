﻿using System;
using TMPro;
using UnityEngine;

public class FoodPosition : MonoBehaviour
{
    [SerializeField] private int maxFoodNumber = 3;
    private int actualFoodNumber = 0;
    
    public Food.FoodType foodType;

    [SerializeField] private TextMeshProUGUI numberText = null;

    public bool AddFood()
    {
        if (actualFoodNumber >= maxFoodNumber) return false;

        actualFoodNumber++;
        UpdateText();
        return true;
    }

    public bool TakeFood()
    {
        if (actualFoodNumber <= 0) return false;

        actualFoodNumber--;
        UpdateText();
        return true;
    }

    private void UpdateText()
    {
        numberText.text = actualFoodNumber + "/" + maxFoodNumber;
    }
}
