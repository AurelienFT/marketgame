﻿using System.Collections.Generic;
using FrancoisSauce.Scripts.FSUtils;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoxOpened : IInteractible
{
    [SerializeField] private Box boxScript;
    public int foodNumber = 1;

    public GameObjectPool foodPool = null;
    public List<Food.FoodType> availableFoodTypes = null;

    public override void InteractStart(Transform player)
    {
        foodNumber--;

        var newFood = foodPool.RequestPool().GetComponent<Food>();

        var transformPlayer = player.transform;
        newFood.transform.position = transformPlayer.position + transformPlayer.forward;
        player.GetComponent<PlayerInteraction>().currentInteractionSelected = newFood;
        newFood.SetFoodType(availableFoodTypes[Random.Range(0, availableFoodTypes.Count)]);
        newFood.InteractStart(player);
        InteractEnd();
    }

    public override bool InteractStay()
    {
        //Shouldn't be called
        return false;
    }

    public override void InteractEnd()
    {
        //Should only be called in private
        if (foodNumber >= 0) return;

        gameObject.SetActive(false);
        boxScript.enabled = true;
        enabled = false;
        boxScript.boxPosition.isEmpty = true;
    }

    public override void InteractAction()
    {
        //Shouldn't be called
        ;
    }
}