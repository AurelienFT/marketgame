﻿using System;
using System.Collections;
using System.Collections.Generic;
using FrancoisSauce.Scripts.FSUtils;
using UnityEngine;

public class CustomerHappinessSmiley : MonoBehaviour
{
    [SerializeField] private List<GameObject> faces = new List<GameObject>();

    private void OnEnable()
    {
        foreach (var face in faces)
        {
            face.SetActive(false);
        }

        faces[3].SetActive(true);
    }

    public void HappinessMultiplierChanged(int value)
    {
        foreach (var face in faces)
        {
            face.SetActive(false);
        }

        faces[value].SetActive(true);
    }
}
