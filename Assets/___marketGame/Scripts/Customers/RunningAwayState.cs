﻿using System.Runtime.InteropServices;
using UnityEngine;

public class RunningAwayState : ICustomerState
{
    private static readonly int Walk = Animator.StringToHash("Walk");

    public override void Start(CustomerStateManager customerStateManager)
    {
        customerStateManager.myAnimator.SetBool(Walk, true);
        customerStateManager.MoveCustomer(Vector3.back * 10);
        customerStateManager.navMeshAgent.speed = 10;
    }

    public override void Update(CustomerStateManager customerStateManager)
    {
    }

    public override void Exit(CustomerStateManager customerStateManager)
    {
    }
}