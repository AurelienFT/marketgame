﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Random = UnityEngine.Random;

public class WalkingCustomerState : ICustomerState
{
    private List<Transform> goal = new List<Transform>();
    
    private float currentTimeAtGoal = 0f;
    private int currentGoal = 0;

    private bool isGoaling = false;

    private float timeToSpawnDirtyPart = 0;
    private float currentTimeToSpawnDirtyPart = 0;
    private static readonly int Walk = Animator.StringToHash("Walk");

    public override void Start(CustomerStateManager customerStateManager)
    {
        var usedGoals = new List<int>();

        for (; goal.Count < customerStateManager.goalNumber;)
        {
            var tmp = Random.Range(0, customerStateManager.customerEndPoints.Count);
            if (usedGoals.Contains(tmp)) {
                continue;
            }
            goal.Add(customerStateManager.customerEndPoints[tmp]);
            usedGoals.Add(tmp);
        }

        timeToSpawnDirtyPart = Random.Range(customerStateManager.minTimeSpawnOopsies,
            customerStateManager.maxTimeSpawnOopsies);
    }

    public override void Update(CustomerStateManager customerStateManager)
    {
        //GO TO GOAL PART/ STOP WHEN GOALING PART
        if (!isGoaling)
        {
            if (customerStateManager.navMeshAgent.remainingDistance <= 2.01f)
            {
                isGoaling = true;
                customerStateManager.PickUpFood();
                customerStateManager.myAnimator.SetBool(Walk, false);
            }
        }

        //GOALING PART/ RESET TO WALKING AROUND
        if (isGoaling)
        {
            currentTimeAtGoal += Time.deltaTime;

            if (currentTimeAtGoal >= customerStateManager.timeSpentAtGoal)
            {
                currentTimeAtGoal = 0;
                if (currentGoal >= customerStateManager.goalNumber)
                {
                    customerStateManager.ChangeState(customerStateManager.quittingState);
                    return;
                }
                customerStateManager.navMeshAgent.destination = goal[currentGoal].position;
                customerStateManager.myAnimator.SetBool(Walk, true);
                currentGoal++;
                isGoaling = false;
            }
        }

        currentTimeToSpawnDirtyPart += Time.deltaTime;
        if (currentTimeToSpawnDirtyPart < timeToSpawnDirtyPart) return;

        currentTimeToSpawnDirtyPart = 0f;
        timeToSpawnDirtyPart = Random.Range(
            customerStateManager.minTimeSpawnOopsies,
            customerStateManager.maxTimeSpawnOopsies
        );
        
        var newBottle = customerStateManager.dirtyPartPool.RequestPool();
        var transform = customerStateManager.transform;
        newBottle.transform.position = transform.position + (transform.right * 2);
    }

    public override void Exit(CustomerStateManager customerStateManager)
    {
        currentGoal = 0;
        currentTimeAtGoal = 0;
        isGoaling = false;
    }
}
