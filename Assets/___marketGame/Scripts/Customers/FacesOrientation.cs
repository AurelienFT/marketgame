﻿using System;
using UnityEngine;

public class FacesOrientation : MonoBehaviour
{
    [SerializeField] private Transform mainCameraTransform = null;

    private void OnEnable()
    {
        if (Camera.main != null) mainCameraTransform = Camera.main.transform;
    }

    private void LateUpdate()
    {
        transform.LookAt(mainCameraTransform);
    }
}
