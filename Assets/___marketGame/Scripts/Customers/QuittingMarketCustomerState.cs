﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuittingMarketCustomerState : ICustomerState
{
    private static readonly int Walk = Animator.StringToHash("Walk");

    public override void Start(CustomerStateManager customerStateManager)
    {
        customerStateManager.navMeshAgent.destination = customerStateManager.checkoutManager.AddToCheckout(customerStateManager);
    }

    public override void Update(CustomerStateManager customerStateManager)
    {
        if (!(customerStateManager.navMeshAgent.remainingDistance < 2.01f)) return;
        
        if (customerStateManager.myAnimator.GetBool(Walk))
            customerStateManager.myAnimator.SetBool(Walk, false);
        customerStateManager.UpdateHappiness(-Time.deltaTime * 3);

    }

    public override void Exit(CustomerStateManager customerStateManager)
    {
        ;
    }
}
