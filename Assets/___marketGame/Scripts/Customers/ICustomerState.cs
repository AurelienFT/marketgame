﻿public abstract class ICustomerState
{
    public abstract void Start(CustomerStateManager customerStateManager);
    public abstract void Update(CustomerStateManager customerStateManager);
    public abstract void Exit(CustomerStateManager customerStateManager);
}
