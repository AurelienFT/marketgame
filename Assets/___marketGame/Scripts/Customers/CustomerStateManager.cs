﻿using System;
using System.Collections;
using System.Collections.Generic;
using FrancoisSauce.Scripts.FSEvents.SO;
using FrancoisSauce.Scripts.FSUtils;
using UnityEngine;
using UnityEngine.AI;

public class CustomerStateManager : MonoBehaviour
{
    private ICustomerState currentState = null;
    
    public readonly WalkingCustomerState walkingState = new WalkingCustomerState();
    public readonly QuittingMarketCustomerState quittingState = new QuittingMarketCustomerState();
    public readonly RunningAwayState runningAwayState = new RunningAwayState();

    public GameObjectPool dirtyPartPool = null;

    public List<Transform> customerEndPoints = new List<Transform>();

    public int goalNumber = 2;
    public float timeSpentAtGoal = 3f;
    public NavMeshAgent navMeshAgent;

    [SerializeField] private FSVoidEventSO onClientLeaveCheckout = null;

    public CheckoutManager checkoutManager;
    [HideInInspector] public int checkoutIndex = 0;

    public float minTimeSpawnOopsies = 0f;
    public float maxTimeSpawnOopsies = 0f;

    private FoodPosition currentFoodPosition = null;
    [HideInInspector] public int foodNumber = 0;

    [HideInInspector] public int happinessMultiplier = 3;
    private float happiness = 100;

    [SerializeField] private CustomerHappinessSmiley smiley = null;
    
    [SerializeField] private FSVoidEventSO onCustomerLeaveCheckout = null;
    [SerializeField] private FSIntEventSO onScoreChanged = null;

    public Animator myAnimator = null;
    private static readonly int Walk = Animator.StringToHash("Walk");

    private IEnumerator Enabling()
    {
        yield return new WaitForEndOfFrame();
        
        if (goalNumber > customerEndPoints.Count)
            throw new System.ArgumentException("NTM TU AS TROP DE GOAL NUMBER PAR RAPPORT AUX POINTS", "SALMERD");;

        currentState = walkingState;
        currentState.Start(this);
        navMeshAgent.speed = 3.5f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 13)//Food position layer
        {
            currentFoodPosition = other.GetComponent<FoodPosition>();
        }

        if (other.gameObject.layer == 9) //Interactible layer => Only dirt part because they are the only one that are triggered
        {
            UpdateHappiness(-10);
        }
       
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer != 13) return; //Food position layer

        currentFoodPosition = null;
    }

    private void OnEnable()
    {
        StartCoroutine(Enabling());
    }

    public void OnUpdateGameStarted()
    {
        currentState.Update(this);
    }

    public void ChangeState(ICustomerState newState)
    {
        currentState.Exit(this);
        currentState = newState;
        currentState.Start(this);
    }

    public IEnumerator Quitting()
    {
        yield return new WaitForSecondsRealtime(3f);
        onCustomerLeaveCheckout.Invoke();
        gameObject.SetActive(false);
    }
    
    public void UpdateHappiness(float happinessValue)
    {
        if (happiness <= 0) return;
        happiness += happinessValue;

        if (happiness <= 0)
        {
            smiley.HappinessMultiplierChanged(0);
            ChangeState(runningAwayState);
            onScoreChanged.Invoke(-50);
            StartCoroutine(Quitting());
            return;
        }

        var tmp = (int)happiness / 33;
        tmp++;
        if (tmp >= 4)
            tmp = 3;

        if (tmp == happinessMultiplier) return;
        
        happinessMultiplier = tmp;
        smiley.HappinessMultiplierChanged(happinessMultiplier);
    }

    public void MoveCustomer(Vector3 newDestination)
    {
        myAnimator.SetBool(Walk, false);
        navMeshAgent.destination = newDestination;
    }

    private IEnumerator WaitingToPickUpFood()
    {
        yield return new WaitForSeconds(1f);

        if (!currentFoodPosition) yield break;
        
        if (currentFoodPosition.TakeFood())
        {
            foodNumber++;
        }
        else
        {
            UpdateHappiness(-10);
        }

    }
    
    public void PickUpFood()
    {
        StartCoroutine(WaitingToPickUpFood());
    }

    public void CustomerLeaveCheckout()
    {
        onClientLeaveCheckout.Invoke();
    }
}
