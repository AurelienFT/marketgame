﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Station : MonoBehaviour
{
    public bool isUnlocked = true;
    public Station leftStation = null;
    public Station rightStation = null;
    public Station upStation = null;
    public Station downStation = null;
    public int indexLevel = 0;
}
