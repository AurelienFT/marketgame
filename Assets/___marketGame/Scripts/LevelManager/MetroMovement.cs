﻿using UnityEngine;
using DG.Tweening;
using FrancoisSauce.Scripts.FSEvents.SO;
using FrancoisSauce.Scripts.FSSingleton;

public class MetroMovement : MonoBehaviour
{
    public Station currentStation;
    public bool isMoving;

    [SerializeField] private FSIntEventSO onLevelSelectionChanged = null;

    private Transform myTransform;

    private void Awake()
    {
        myTransform = transform;
    }

    private void Start()
    {
        onLevelSelectionChanged.Invoke(currentStation.indexLevel);
    }

    private void OnEnable()
    {
        hasStarted = false;
    }

    public void Commands(MultiplayerHandler.PlayerInput input, Vector3 value, int id)
    {
        switch (input)
        {
            case MultiplayerHandler.PlayerInput.Direction:
            {
                Move(value);
                break;
            }
            case MultiplayerHandler.PlayerInput.Action:
            {
                OnValidateMetro();
                break;
            }
            default:
                break;
        }
    }

    private bool hasStarted = false;

    public void OnValidateMetro()
    {
        if (hasStarted) return;

        hasStarted = true;
        StartCoroutine(GameManager.Instance.LoadSceneAsync(currentStation.indexLevel, true, false));
    }

    // TODO : Possible do go back when trailling
    public void Move(Vector3 direction)
    {
        if (isMoving) return;
        
        if (direction.x > -.3f && direction.x < .3f && direction.z > .7f && currentStation.upStation != null)
        {
            MoveMetro(currentStation.upStation.transform.position, new Vector3(0, -90, 90), currentStation.upStation);
        }
        if (direction.x > -.3f && direction.x < .3f && direction.z < -.7f && currentStation.downStation != null)
        {
            MoveMetro(currentStation.downStation.transform.position, new Vector3(0, 90, -90), currentStation.downStation);
        }
        if (direction.x < -.7f && direction.z > -.3f && direction.z < .3f && currentStation.leftStation != null)
        {
            MoveMetro(currentStation.leftStation.transform.position, new Vector3(90, 0, 180), currentStation.leftStation);
        }
        if (direction.x > .7f && direction.z > -.3f && direction.z < .3f && currentStation.rightStation != null)
        {
            MoveMetro(currentStation.rightStation.transform.position, new Vector3(-90, 0, 0), currentStation.rightStation);
        }
    }

    private void MoveMetro(Vector3 positionToMoveTo, Vector3 rotation, Station newCurrentStation)
    {
        isMoving = true;
        myTransform.eulerAngles = rotation;
        myTransform.DOMove(positionToMoveTo, 1.0f)
            .onComplete += () => isMoving = false;
        currentStation = newCurrentStation;
        onLevelSelectionChanged.Invoke(currentStation.indexLevel);
    }

    private void Update()
    {
        var device = InControl.InputManager.ActiveDevice;

        if (device.Direction.IsPressed && device.Direction.HasChanged)
        {
            var tmp = device.LeftStick.Value;
            MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Direction, new Vector3(tmp.x, 0, tmp.y), 0);
        }

        if (device.Action1.IsPressed && device.Action1.HasChanged)
        {
            MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Action, Vector3.zero, 0);
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Direction, Vector3.back, 0);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Direction, Vector3.forward, 0);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Direction, Vector3.left, 0);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Direction, Vector3.right, 0);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Action, Vector3.zero, 0);
        }
    }
}
