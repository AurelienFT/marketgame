﻿using System.Linq;
using UnityEngine;

public class EndOfLevel : MonoBehaviour
{
    [SerializeField] private LevelSO levelSettings = null;

    public void OnTimeUp(int score)
    {
        if (score < PlayerPrefs.GetInt(levelSettings.levelName)) return;
        PlayerPrefs.SetInt(levelSettings.levelName, score);
    }
}
