﻿using System.Collections.Generic;
using FrancoisSauce.Scripts.FSUtils;
using UnityEngine;

public class BoxGenerator : MonoBehaviour
{
    [SerializeField] private GameObjectPool poolBox = null;
    [SerializeField] private GameObjectPool poolFood = null;
    
    public Transform CreateBox(Vector3 startingPosition, int fruitNumberInBox, List<Food.FoodType> availableFoodTypes)
    {
        var newBox = poolBox.RequestPool().GetComponent<BoxOpened>();
        
        var transformBox = newBox.transform;
        transformBox.position = startingPosition;
        transformBox.rotation = Quaternion.identity;
        newBox.foodNumber = fruitNumberInBox;
        newBox.foodPool = poolFood;
        newBox.availableFoodTypes = availableFoodTypes;

        return transformBox;
    }
}
