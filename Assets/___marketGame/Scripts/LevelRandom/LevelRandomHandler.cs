﻿using UnityEngine;
using Random = UnityEngine.Random;

public class LevelRandomHandler : MonoBehaviour
{
    [SerializeField] private LevelSO levelSettings = null;

    [SerializeField] private Transform boxStartingPosition = null;
    [SerializeField] private BoxGenerator boxGenerator = null;
    private Transform lastBox = null;
    private int currentBoxNumber = 0;

    [SerializeField] private Transform upperBoundCustomer = null;
    [SerializeField] private Transform lowerBoundCustomer = null;
    [SerializeField] private Transform rightBoundCustomer = null;
    [SerializeField] private Transform leftBoundCustomer = null;
    [SerializeField] private CustomerGenerator customerGenerator = null;
    private int currentCustomerNumber = 0;

    private float timeToSpawnCustomer = 0f;
    private float currentTimeSpawnCustomer = 0f;
    
    private float timeToSpawnEvent = 0f;
    private float currentTimeSpawnEvent = 0f;

    [SerializeField] private UILevelManager uiLevelManager = null;

    private void Start()
    {
        lastBox = boxGenerator.CreateBox(
            boxStartingPosition.position,
            Random.Range(levelSettings.minFoodNumberInBox, levelSettings.maxFoodNumberInBox),
            levelSettings.availableFoodTypes
        );
        levelSettings.SetUpList();

        timeToSpawnCustomer =
            Random.Range(levelSettings.minTimeCustomerSpawnRate, levelSettings.maxTimeCustomerSpawnRate);
        timeToSpawnEvent =
            Random.Range(levelSettings.minTimeEventSpawnRate, levelSettings.maxTimeEventSpawnRate);

        uiLevelManager.levelTime = levelSettings.levelDuration;
    }

    private void AddBox()
    {
        if (currentBoxNumber >= levelSettings.boxNumber) return;
        if (!(Vector3.Distance(lastBox.position, boxStartingPosition.position) >= 1)) return;
        
        currentBoxNumber++;
        lastBox = boxGenerator.CreateBox(
            boxStartingPosition.position, 
            Random.Range(levelSettings.minFoodNumberInBox, levelSettings.maxFoodNumberInBox),
            levelSettings.availableFoodTypes
        );
    }

    public void OnCustomerLeaveShop()
    {
        currentCustomerNumber--;
    }

    private void AddCustomer()
    {
        if (currentCustomerNumber >= levelSettings.maxCustomerNumberAtSameTime) return;
        currentTimeSpawnCustomer += Time.deltaTime;
        if (currentTimeSpawnCustomer < timeToSpawnCustomer) return;

        currentCustomerNumber++;
        
        var newPosition = new Vector3(
            Random.Range(
                leftBoundCustomer.position.x, rightBoundCustomer.position.x
            ),
            .75f,
            Random.Range(
                lowerBoundCustomer.position.z, upperBoundCustomer.position.z
            )
        );

        customerGenerator.CreateCustomer(
            newPosition, levelSettings.minTimeCustomerOopsiesRate,
            levelSettings.maxTimeCustomerOopsiesRate
        );
        currentTimeSpawnCustomer = 0f;
        timeToSpawnCustomer =
            Random.Range(levelSettings.minTimeCustomerSpawnRate, levelSettings.maxTimeCustomerSpawnRate);
    }

    private void StartEvent()
    {
        if (levelSettings.isEventOccuring) return;
        currentTimeSpawnEvent += Time.deltaTime;
        if (currentTimeSpawnEvent < timeToSpawnEvent) return;

        StartCoroutine(levelSettings.events[Random.Range(0, levelSettings.events.Count)]());
        currentTimeSpawnEvent = 0f;
        timeToSpawnEvent =
            Random.Range(levelSettings.minTimeEventSpawnRate, levelSettings.maxTimeEventSpawnRate);
    }

    public void OnUpdateGameStarted()
    {
        AddBox();
        AddCustomer();
        StartEvent();
    }

    private void OnDestroy()
    {
        levelSettings.ResetObject();
    }
}
