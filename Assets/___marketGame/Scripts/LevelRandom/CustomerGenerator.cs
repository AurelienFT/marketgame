﻿using System.Collections.Generic;
using FrancoisSauce.Scripts.FSUtils;
using UnityEngine;

public class CustomerGenerator : MonoBehaviour
{
    [SerializeField] private GameObjectPool pool = null;

    [SerializeField] private List<Transform> customerEndPoints = null;
    [SerializeField] private CheckoutManager checkoutManager = null;
    [SerializeField] private GameObjectPool dirtypartPool = null;
    
    public void CreateCustomer(Vector3 startingPosition, float minTimeSpawnOopsies, float maxTimeSpawnOopsies)
    {
        var newCustomer = pool.RequestPool().GetComponent<CustomerStateManager>();
        
        //TODO set infos here
        newCustomer.minTimeSpawnOopsies = minTimeSpawnOopsies;
        newCustomer.maxTimeSpawnOopsies = maxTimeSpawnOopsies;
        newCustomer.customerEndPoints = customerEndPoints;
        newCustomer.checkoutManager = checkoutManager;
        newCustomer.dirtyPartPool = dirtypartPool;
        var transform1 = newCustomer.transform;
        transform1.position = startingPosition;
        transform1.rotation = Quaternion.identity;
    }
}
