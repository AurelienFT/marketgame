﻿using FrancoisSauce.Scripts.FSUtils.FSGlobalVariables;
using InControl;
using UnityEngine;
using Steamworks;

public class PlayerInputs : MonoBehaviour
{
    [SerializeField] private FSGlobalBoolSO isOnline = null;
    private Transform myTransform;

    public int id = 1;

    [SerializeField] private FSGlobalFloatSO playerSpeed = null;

    [SerializeField] private FSGlobalListUlongSO playersIds = null;

    public InputDevice device = null;

    public ulong playerId;

    private bool hasSentVector0 = false;

    private void Awake()
    {
        myTransform = transform;
        if (isOnline.value)
        {
            if (playersIds.value.Count < id + 1)
                gameObject.SetActive(false);
            else {
                playerId = playersIds.value[id];
            }
        }
        else
        {
            if (CrazyShop.InputManager.Instance.playerDevicesNames.Count < id + 1)
                gameObject.SetActive(false);
            else
                device = CrazyShop.InputManager.Instance.playerDevicesNames[id];
        }
    }
    
    public void OnUpdateGameStarted()
    {
        if (isOnline.value && new CSteamID(playerId) != SteamUser.GetSteamID()) return;

        var direction = Vector3.zero;
        if (isOnline.value && (device == null || device.Name == "None")) {
            device = InControl.InputManager.ActiveDevice;
        }

        if (device != null)
        {
            if (device.Direction.IsPressed)
            {
                var tmp = device.LeftStick.Value;
                direction = new Vector3(tmp.x, 0, tmp.y);
            }
    
            if (device.Action1.IsPressed && device.Action1.HasChanged)
            {
                MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Action, Vector3.zero, id);
            }

            if (device.Action2.IsPressed && device.Action2.HasChanged)
            {
                MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Dash, Vector3.zero, id);
            }
        }
        if (device == null || isOnline.value)
        {
            if (Input.GetKey(KeyCode.DownArrow))
            {
                direction += Vector3.back;
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                direction += Vector3.forward;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                direction += Vector3.left;
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                direction += Vector3.right;
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Action, Vector3.zero, id);
            }

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Dash, Vector3.zero, id);
            }
        }

        if (direction != Vector3.zero || !hasSentVector0)
        {
            MultiplayerHandler.Instance.ReceiveInputs(MultiplayerHandler.PlayerInput.Direction, direction * playerSpeed.value * Time.deltaTime, id);
        }
        hasSentVector0 = direction == Vector3.zero;
    }
}
