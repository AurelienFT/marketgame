﻿using System;
using System.Collections;
using DG.Tweening;
using FrancoisSauce.Scripts.FSUtils.FSGlobalVariables;
using UnityEditor;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Transform myTransform;

    [SerializeField] private FSGlobalFloatSO playerDashSpeed = null;
    [SerializeField] private Animator myAnimator = null;

    [SerializeField] private FSGlobalBoolSO isOnline = null;

    [SerializeField] private FSGlobalBoolSO isServer = null;


    private bool isDashing = false;

    public int id = 1;
    
    private void Awake()
    {
        myTransform = transform;
    }
    
    public void Commands(MultiplayerHandler.PlayerInput input, Vector3 value, int id)
    {
        //print("steam id 2 should be all " + id + " of " + gameObject.name);
        if (id != this.id) return;
        
        switch (input)
        {
            //print("steam id 2 should be 1 " + id + " of " + gameObject.name);
            case MultiplayerHandler.PlayerInput.Direction when !isDashing:
            {
                myAnimator.SetBool("Run", value != Vector3.zero);
                if (value == Vector3.zero) return;
                DOTween.Kill(myTransform);
                var position = myTransform.position;
                position += value;
                if (!isOnline.value || isServer.value)
                {
                    myTransform.position = position;
                }
                myTransform.DOLookAt(position + value, .1f);
                break;
            }
            case MultiplayerHandler.PlayerInput.Dash:
            {
                if (isOnline.value && !isServer.value)
                {
                    break;
                }
                if (!isDashing)
                {
                    StartCoroutine(Dash());
                }

                break;
            }
            case MultiplayerHandler.PlayerInput.Action:
                break;
            default:
                print("should not happen but it's okay");
                break;
        }
    }
    
    private IEnumerator Dash()
    {
        isDashing = true;
        var direction = myTransform.forward;
        
        for (var i = 0f; i < .1f; i += Time.deltaTime)
        {
            myTransform.position += direction * (playerDashSpeed.value * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }

        isDashing = false;
    }
}
