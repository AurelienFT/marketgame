﻿using System;
using System.Collections;
using System.Linq;
using RootMotion.FinalIK;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    [HideInInspector] public IInteractible currentInteractionSelected = null;
    private bool isInteracting = false;

    public BipedIK bipedIk = null;

    [SerializeField] private ParticleSystem myImpactParticles = null;
    [SerializeField] private Rigidbody myRigidbody = null;

    //TODO changer les id qui sont partout différents (sûrement avec un globalBool)
    public int id = 1;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer != 11 && other.gameObject.layer != 8) return; // player and customer layer
        
        //TODO adding force not working
        //myRigidbody.AddForce((other.transform.position - transform.position) * 10000);
        myImpactParticles.Play();
    }

    private void OnTriggerStay(Collider other)
    {
        if (currentInteractionSelected != null) return;
        
        if (other.gameObject.layer != 9) return;
        
        currentInteractionSelected = other.GetComponent<IInteractible>();
        if (currentInteractionSelected.enabled == false)
        {
            currentInteractionSelected = other.GetComponents<IInteractible>()[1];
        }
        
        if (!currentInteractionSelected.isInteracted) return;
        
        currentInteractionSelected = null;
    }

    private void OnTriggerExit(Collider other)
    {
        if (currentInteractionSelected == null) return;
        if (other.gameObject.layer != 9) return; //Interactible layer
        if (!other.GetComponents<IInteractible>().Contains(currentInteractionSelected)) return;
        
        if (isInteracting)
            currentInteractionSelected.InteractEnd();
        currentInteractionSelected = null;
        isInteracting = false;
    }

    private void SetInteracting()
    {
        if (isInteracting) return;
        isInteracting = true;
    }

    private IEnumerator ResetCurrentInteraction()
    {
        yield return new WaitForEndOfFrame();
        currentInteractionSelected = null;
    }
    
    public void OnUpdateGameStarted()
    {
        if (!isInteracting) return;
        
        if (currentInteractionSelected.InteractStay()) return;
        
        currentInteractionSelected.InteractEnd();
        isInteracting = false;
        StartCoroutine(ResetCurrentInteraction());
    }

    public void Commands(MultiplayerHandler.PlayerInput input, Vector3 value, int id)
    {
        if (id != this.id) return;
        if (input != MultiplayerHandler.PlayerInput.Action) return;
        if (currentInteractionSelected == null) return;
        if (isInteracting)
        {
            currentInteractionSelected.InteractAction();
        }
        else
        {
            currentInteractionSelected.InteractStart(transform);
            SetInteracting();
        }
    }
}
